//<Juhász Norbert BH08>;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh08zh01;

/**
 *
 * @author Norbi
 */
public class JuneMonth {

    public static void main(String[] args) {

        int[] array = new int[30];
        print(array);
        System.out.print("Lowest Temperature is: ");
        lowestTemp(array);
        System.out.print("June average Temperature is: ");
        avgTemp(array);
        System.out.println("June workdays average Temperature is: ");
        workdaysAvgTemp(array);
        System.out.print("June highest Temperature is on: ");
        highestTemp(array);

    }

    public static void print(int[] array) {
        System.out.println("array numbers: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10 + 20);
            System.out.print(array[i] + ", ");

        }
        System.out.println("");
    }

    public static void lowestTemp(int[] array) {
        int low = 100;
        for (int i = 0; i < array.length; i++) {
            if (low > array[i]) {
                low = array[i];
            }

        }
        System.out.println(low);
    }

    public static void avgTemp(int[] array) {

        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];

        }
        double avg = sum / array.length;
        System.out.println(avg);
    }

    public static void workdaysAvgTemp(int[] array) {
        double sum = 0;
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 7 == 0) {
                System.out.println("Wednesday:" + array[i]);
            }
            if (i % 7 == 1) {
                System.out.println("Thursday:" + array[i]);
            }
            if (i % 7 == 2) {
                System.out.println("Friday:" + array[i]);
            }
            if (i % 7 == 5) {
                System.out.println("Monday:" + array[i]);
            }
            if (i % 7 == 6) {
                System.out.println("Tuesday:" + array[i]);
            }

        }

    }

    public static void highestTemp(int[] array) {
        int high = 0;

        for (int i = 1; i < array.length; i++) {
            if (high < array[i]) {
                high = array[i];
            }
        }
        for (int j = 1; j < array.length; j++) {
            if (high == array[j]) {

                if (j % 7 == 0) {
                    System.out.print("Wednesday, ");
                }
                if (j % 7 == 1) {
                    System.out.print("Thursday, ");
                }
                if (j % 7 == 2) {
                    System.out.print("Friday, ");
                }
                if (j % 7 == 3) {
                    System.out.print("Saturday, ");
                }
                if (j % 7 == 4) {
                    System.out.print("Sunday, ");
                }
                if (j % 7 == 5) {
                    System.out.print("Monday, ");
                }
                if (j % 7 == 6) {
                    System.out.print("Tuesday, ");
                }
            }

        }
    }
}
