//<Juhász Norbert BH08>;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh08zh01;

/**
 *
 * @author Norbi
 */
public class Game {

    public static void main(String[] args) {
        char[][] matrix = new char[12][12];
        char letter = 'c';
        int end = 1;
        arrayLoad(matrix);
        randomChar(matrix);
        randomNumbers(matrix);
        kiir(matrix,end);
        do {

            move(matrix, letter);
            clearScreen();
            kiir(matrix,end);
            
        } while (end == 1);

    }

    public static void arrayLoad(char matrix[][]) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = '.';
                matrix[i][0] = '|';
                matrix[i][11] = '|';

            }

        }

    }

    public static void randomChar(char matrix[][]) {
        for (int i = 0; i < 1; i++) {
            int x = (int) (Math.random() * matrix.length - 1 + 1);
            int y = (int) (Math.random() * matrix.length - 1 + 1);
            matrix[x][y] = 'O';

        }

    }

    public static void randomNumbers(char matrix[][]) {
        for (int i = 0; i < 1; i++) {
            int row = (int) ((Math.random() * matrix.length - 1) + 1);
            int coloumn = (int) ((Math.random() * matrix.length - 1) + 1);
            matrix[row][coloumn] = '1';

        }
    }

    public static void kiir(char matrix[][],int end) {
        System.out.println("-  -  -  -  -  -  -  -  -  -  -  -  ");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "  ");
                 if(matrix[i][j]=='O' && matrix[i][j]=='1')
                end =0;
            }
            System.out.println("");
        }
        System.out.println("-  -  -  -  -  -  -  -  -  -  -  -  ");
    }

    public static void clearScreen() {
        int b = 5;
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
        {
            int a = 5;
            {
                int c = 5;
            }
        }
    }

    public static void move(char matrix[][], char letter) {

        switch (letter = extra.Console.readChar("Press w (up) or a (left) or s (down) or d (right) button.")) {
            case 'w':
                boolean run = true;
                for (int i = 1; i < matrix.length && run; i++) {
                    for (int j = 0; j < matrix[i].length && run; j++) {
                        if (matrix[i][j] == 'O' /*&& i>0*/) {
                            matrix[i - 1][j] = 'O';
                            matrix[i][j] = '.';
                            run = false;

                        }
                    }

                }
                break;
            case 'a':
                run = true;
                for (int i = 0; i < matrix.length && run; i++) {
                    for (int j = 2; j < matrix[i].length && run; j++) {
                        if (matrix[i][j] == 'O' /*&& j>0*/) {
                            matrix[i][j - 1] = 'O';
                            matrix[i][j] = '.';
                            run = false;
                        }
                    }

                }
                break;
            case 's':
                run = true;
                for (int i = 0; i < matrix.length - 1 && run; i++) {
                    for (int j = 0; j < matrix[i].length && run; j++) {
                        if (matrix[i][j] == 'O') {
                            matrix[i + 1][j] = 'O';
                            matrix[i][j] = '.';
                            run = false;
                        }
                    }

                }
                break;
            case 'd':
                run = true;
                for (int i = 0; i < matrix.length && run; i++) {
                    for (int j = 0; j < matrix[i].length - 2 && run; j++) {
                        if (matrix[i][j] == 'O') {
                            matrix[i][j + 1] = 'O';
                            matrix[i][j] = '.';
                            run = false;
                        }
                    }

                }
                break;
            default:
                break;
        }
       
    }
}
