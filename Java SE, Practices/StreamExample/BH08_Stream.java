/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh08_stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Norbi
 */
public class BH08_Stream {

    static class Person {

        String name;
        int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person{" + "name=" + name + ", age=" + age + '}';
        }

    }

    public static void main(String[] args) {
        // TODO code application logic here

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Z", 0));
        persons.add(new Person("AZ", 12));
        persons.add(new Person("MZ", 25));
        persons.add(new Person("ZPS", 4));
        persons.add(new Person("PLWSZ", 12));
        persons.add(new Person("PPPZ", 98));
        persons.add(new Person("POLI", 12));
        persons.add(new Person("MMUIOK", 5));
        persons.add(new Person("MMUIOK", 5));

        //filter
        List<Person> filtered = persons.stream()
                .filter(item -> item.age > 15)
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());
        System.out.println(filtered);

        //filter - LinkedList
        LinkedList<Person> filteredIntoLinkedList = persons.stream()
                .filter(item -> item.age > 15)
                .collect(Collectors.toCollection(LinkedList::new));

        //map - neve + 'Z' az elejére
        List<String> transformed = persons.stream()
                .map(item -> "Z" + item.getName())
                .collect(Collectors.toList());

        //map - neve
        List<String> names = persons.stream()
                .map(Person::getName)
                .collect(Collectors.toList());

        System.out.println(names);

        /*  Map<Integer, List<Person>> personsInMap = persons.stream()
               .collect(Collectors.toMap(
                       keyMapper, valueMapper))
         */
        int sum = persons.stream()
                .map(Person::getAge)
                .reduce(
                        0,
                        (a, b) -> a + b,
                        (a, b) -> a + b
                );
        System.out.println(sum);

        Map<Boolean, List<Person>> partitions = persons.stream()
                .collect(Collectors.partitioningBy(
                        person -> person.getAge() < 15
                ));

        System.out.println("Partitions:" + partitions);

        System.out.println("\nHomework\n");
        //     1) Adott egy String, ami csak + és - jeleket tartalmaz. 
        //      Számoljuk meg, hány + és - jel van benne (pl.: partitioning és size).
        System.out.println("First exercise: ");
        String signal = "+++--+--++--";

        Map<Character, Long> plusMinusSignals = signal.chars().mapToObj(e -> (char) e)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        System.out.println("+ and - chars:  " + plusMinusSignals);

        //    2) Írjuk ki név szerint rendezve azokat, akiknek az életkora páratlan.       
        System.out.println("Second exercise: ");

        LinkedList<Person> oddNumbers = persons.stream()
                .filter(item -> item.getAge() % 2 == 1)
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toCollection(LinkedList::new));

        System.out.println(oddNumbers);

        /*       3) List<Person>-ből legyen Map<Integer, String>.
- key: életkor
- value: a person neve
- ütközés esetén a nevek legyenek összefűzve
- implementáció: mindegy*/
        System.out.println("Third exercise: ");

        Map<Integer, String> peopleMap = persons.stream().collect(Collectors.toMap(
                person -> person.getAge(),
                person -> person.getName(),
                (firstString, secondString) -> {
                    return firstString.concat(secondString);
                },
                HashMap::new
        ));
        System.out.println(peopleMap);

        //      4) Reduce-t használva adjuk meg a páratlan életkorok összegét (életkorok, amelyek páratlanok).
        System.out.println("Fourth exercise: ");

        double sumOfOddAges = persons.stream()
                .filter(item -> item.getAge() % 2 == 1)
                .map(Person::getAge)
                .reduce(
                        0,
                        (a, b) -> a + b,
                        (a, b) -> a + b
                );

        System.out.println("Odd ages sum is: " + sumOfOddAges);

        //     5) Próbáljátok ki distinct(), skip(), limit() műveleteket 1-1 példával.
        System.out.println("Fifth exercise: ");

        System.out.println("Distinct persons:");

        persons.stream()
                .distinct()
                .forEach(System.out::print);

        System.out.println("\n Limit 5 :");

        persons.stream()
                .map(Person::getName)
                .limit(5)
                .forEach(System.out::print);

        System.out.println("\n Skip 6 :");

        persons.stream()
                .map(Person::getName)
                .skip(6)
                .forEach(System.out::print);

    }
}
