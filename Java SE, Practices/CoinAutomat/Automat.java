/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haziokt9;

/**
 *
 *
 * @author Norbi
 */
public class Automat {

    public static void main(String[] args) {

        int coin = 0;
        int target = 0;
        int sum = 0;

        coinMachine(coin, target, sum);
    }

    public static void coinMachine(int coin, int target, int sum) {
        do {
            target = extra.Console.readInt("Target value: (only 10 ending numbers) ");
        } while (target % 10 != 0 || target < 0);
        do {
            do {

                coin = extra.Console.readInt("Insert coin: (10,20,50 or 100) ");

            } while (coin != 100 && coin != 50 && coin != 20 && coin != 10);
            target = target - coin;
            if (target > 0) {
                System.out.println("The remaining amount is: " + target);
            }
        } while (target > 0);
        if (target < 0) {
            target = Math.abs(target);
        }
        System.out.println("Cash back: " + target);

    }
}
