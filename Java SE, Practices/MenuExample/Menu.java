/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package okt2;

/**
 *
 * @author Norbi
 */
public class Menu {

    public static void main(String[] args) {
        int input = -1;
        while (input != 0) {
            printMenu();
            input = extra.Console.readInt();
            switch (input) {
                case 0:
                    System.out.println("Kilepes");
                    break;
                case 1:
                    SumOfDigits();
                    break;
                case 2:
                    NearestPrime(6);
                    break;
                case 3:
                    ketSzamKozottiEgeszek();
                    break;
                case 4:
                    ElsoNOsszege();
                    break;
                case 5:
                    KetSzamKoztiPrimek();
                    break;
                case 6:
                    Grafika(5, 3, '+');
                    Grafika(3, 10, '*');
                    break;
                case 7:
                    Grafika2();
                    break;
                case 8:
                    ackermann(1,2);

                    break;
                case 9:
                    long min = hanoi(6);
                    System.out.println("Minimal step number: " + min);
                   
                    break;

            }
        }
    }

    public static void printMenu() {
        System.out.println("");
        System.out.println("1 - 7.3.5 Feladat");//ok
        System.out.println("2 - 7.3.6 Feladat");//ok
        System.out.println("3 - 7.3.8 Feladat");//ok
        System.out.println("4 - 7.3.13 Feladat");//ok
        System.out.println("5 - 7.3.15 Feladat");//ok
        System.out.println("6 - 7.3.16 Feladat");//ok
        System.out.println("7 - 7.3.17 Feladat");
        System.out.println("8 - 7.3.18 Feladat");
        System.out.println("9 - 7.3.19 Feladat");
        System.out.println("10 - Döntsük el egy számról hogy palindrom");
        System.out.println("0 - Kilepes");

    }

    public static void SumOfDigits() {
        System.out.println("Please write a number: ");
        int num = extra.Console.readInt();
        int maradek = num % 10;
        num = num / 10;
        int szam = num + maradek;
        System.out.println("Sum of digits:");

    }

    public static boolean Prime(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int NearestPrime(int number) {
        int distance = 1;
        boolean IsPrime = false;
        if (Prime(number)) {
            return number;
        }
        do {
            if (Prime(number + distance)) {
                return number + distance;
            } else if (Prime(number - distance)) {
                return number - distance;
            }
            distance++;
        } while (!IsPrime);
        return 0;
    }

    public static void getIntInputFromConsole() {
        int num1 = extra.Console.readInt();
        int num2 = extra.Console.readInt();
        int startInterval = num1 > num2 ? num2 : num1;
        int endInterval = num1 > num2 ? num1 : num2;
        System.out.println(num1 + ", " + num2);
    }

    public static void ketSzamKozottiEgeszek() {
        System.out.println("7.3.8-as feladat: KétSzámKözöttiEgészek1");
        int num1 = extra.Console.readInt("First number: ");
        int num2 = extra.Console.readInt("Second number: ");
        int startInterval = num1 > num2 ? num2 : num1;
        int endInterval = num1 > num2 ? num1 : num2;
        printNumbersFromInterval(startInterval, endInterval);
    }

    public static void printNumbersFromInterval(int start, int end) {
        for (int i = start; i < end; i++) {
            System.out.print(i + ", ");
        }
    }

    public static void ElsoNOsszege() {
        int num = extra.Console.readInt("Write a number: ");
        int sum = 0;
        for (int i = 1; i <= num; i++) {
            sum = i + sum;
        }
        System.out.println("The sum of numbers from 1 to n: " + sum);
    }

    public static void KetSzamKoztiPrimek() {
        int num1 = extra.Console.readInt("Number 1:");
        int num2 = extra.Console.readInt("Number 2:");
        int startInterval = num1 > num2 ? num2 : num1;
        int endInterval = num1 > num2 ? num1 : num2;
        for (int j = startInterval; j < endInterval; j++) {
            boolean prim;

            if (j < 2) {
                prim = false;
            } else {
                int i = 2;
                while (i <= Math.sqrt(j) && j % i != 0) {
                    i++;
                }
                prim = i > Math.sqrt(j);
            }

            if (prim == true) {
                System.out.println(j);
            }

        }
    }

    public static void Grafika(int oszlop, int sor, char betu) {
        for (int i = 1; i <= oszlop; i++) {
            for (int j = 1; j <= sor; j++) {
                System.out.print(betu);
            }
            System.out.println("");

        }
    }

    public static void Grafika2() {
        int oszlop = extra.Console.readInt("Oszlopok száma: ");
        int sor = extra.Console.readInt("Sorok száma: ");
        char betu = extra.Console.readChar("Karakter: ");
        char grafika = extra.Console.readChar("Grafika: ");
        for (int j = 1; j <= sor; j++) {
            System.out.print(betu);
        }
        System.out.println("");

        for (int i = 1; i <= oszlop; i++) {

        }
    }

    public static long ackermann(long num1, long num2) {
        if (num1 == 0) {
            return num2 + 1;
        }
        if (num2 == 0) {
            return ackermann(num1 - 1, 1);
        }
        return ackermann(num1 - 1, ackermann(num1, num2 - 1));

    }

    public static long hanoi(int number) {
        if (number < 1) {
            return 0;
        } else if (number == 1) {
            return 1;
        } else {
            return 2 * hanoi(number - 1) + 1;
        }
    }
    
}
