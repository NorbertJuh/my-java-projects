/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carservice;

/**
 *
 * @author Norbi
 */
public class Customer {
    private String id;
    private String name;
    private boolean platinum;
    private boolean hasLicence;
    private int bankAccount;

    public Customer(String id, String name, boolean platinum, boolean hasLicence, int bankAccount) {
        this.id = id;
        this.name = name;
        this.platinum = platinum;
        this.hasLicence = hasLicence;
        this.bankAccount = bankAccount;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isPlatinum() {
        return platinum;
    }

    public boolean isHasLicence() {
        return hasLicence;
    }

    public int getBankAccount() {
        return bankAccount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlatinum(boolean platinum) {
        this.platinum = platinum;
    }

    public void setHasLicence(boolean hasLicence) {
        this.hasLicence = hasLicence;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }
    
    
}
