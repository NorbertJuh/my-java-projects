/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carservice;

/**
 *
 * @author Norbi
 */
public class Contract {
    private String startDate;
    private String endDate;
    private Car car;
    private Site siteToRetreive;
    private Site siteOfOrigin;
    private Customer customer;

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public Car getCar() {
        return car;
    }

    public Site getSiteToRetreive() {
        return siteToRetreive;
    }

    public Site getSiteOfOrigin() {
        return siteOfOrigin;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setSiteToRetreive(Site siteToRetreive) {
        this.siteToRetreive = siteToRetreive;
    }

    public void setSiteOfOrigin(Site siteOfOrigin) {
        this.siteOfOrigin = siteOfOrigin;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    
    
}

