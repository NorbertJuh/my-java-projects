/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carservice;

/**
 *
 * @author Norbi
 */
public class Car {

    private String plateNumber;
    private String manufacurer;
    private String modelName;
    private String category;
    private int seats;
    private boolean manualGearbox;
    private String engineType;
    private Site site;
    private boolean available;

    public Car(String plateNumber, String manufacurer, String modelName, String category, int seats, boolean manualGearbox, String engineType, Site site, boolean available) {
        this.plateNumber = plateNumber;
        this.manufacurer = manufacurer;
        this.modelName = modelName;
        this.category = category;
        this.seats = seats;
        this.manualGearbox = manualGearbox;
        this.engineType = engineType;
        this.site = site;
        this.available = true;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getManufacurer() {
        return manufacurer;
    }

    public String getModelName() {
        return modelName;
    }

    public String getCategory() {
        return category;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isManualGearbox() {
        return manualGearbox;
    }

    public String getEngineType() {
        return engineType;
    }

    public Site getSite() {
        return site;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public void setManufacurer(String manufacurer) {
        this.manufacurer = manufacurer;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setManualGearbox(boolean manualGearbox) {
        this.manualGearbox = manualGearbox;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public void setSite(Site site) {
        this.site = site;
    }


    
}
