/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carservice;

/**
 *
 * @author Norbi
 */
public class Site {
    private String address;
    private int capacity;
    private Car[] cars;

    public Site(String address, int capacity, Car[] cars) {
        this.address = address;
        this.capacity = capacity;
        this.cars = cars;
    }

    public String getAddress() {
        return address;
    }

    public int getCapacity() {
        return capacity;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
    
    
   public boolean checkAvailable(Car car){
       return car.isAvailable();
   }  
   
public String listAvailable(){
    String listOfAvailableCars = ""; 
        
        for (int i = 0; i < cars.length; i++) {
            listOfAvailableCars += cars[i].toString();
        }        
        return listOfAvailableCars;
    }


    
    
  
    
}
