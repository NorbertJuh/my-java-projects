/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carservice;

/**
 *
 * @author Norbi
 */
public class Company {
    private String name;
    private Site[] sites;
    private Customer[] customers;
    private Contract[] contracts;
    private String address;
    private int taxNumber;
    private int bankaccount;
    private int phonenumber;

    public Company(String name, String address, int taxNumber, int bankaccount, int phonenumber) {
        this.name = name;
        this.address = address;
        this.taxNumber = taxNumber;
        this.bankaccount = bankaccount;
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public Site[] getSites() {
        return sites;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public Contract[] getContracts() {
        return contracts;
    }

    public String getAddress() {
        return address;
    }

    public int getTaxNumber() {
        return taxNumber;
    }

    public int getBankaccount() {
        return bankaccount;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSites(Site[] sites) {
        this.sites = sites;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public void setContracts(Contract[] contracts) {
        this.contracts = contracts;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTaxNumber(int taxNumber) {
        this.taxNumber = taxNumber;
    }

    public void setBankaccount(int bankaccount) {
        this.bankaccount = bankaccount;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        
    }
    
    public Contract createContract(Car car, Customer customer, String startDate, String endDate) {
        Contract contract = new Contract();
        return contract; 
    }    
    
    public boolean checkPlatinum(Customer customer) {
        return customer.isPlatinum(); 
    }
    

}
