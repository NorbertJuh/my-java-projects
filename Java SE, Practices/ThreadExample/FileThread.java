/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileswiththread;

/**
 *
 * @author Norbi
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
    


public class FileThread implements Runnable {

    private BufferedReader inputStream;
    private String fileName;
    List<Integer> list = new ArrayList<>();

    public FileThread(BufferedReader inputStream, String fileName, List<Integer> list) {
        this.inputStream = inputStream;
        this.fileName = fileName;
        this.list=list;
    }

    @Override
    public void run() {
        String line;
        int countRow = 0;
        try {
            while ((line = inputStream.readLine()) != null) {
                countRow++;
            }
        } catch (IOException ex) {

        }
        
        System.out.println(fileName + " contains: " + countRow +  " rows.");
        
        synchronized(list){
            list.add(countRow);
        }
        
    }

}


