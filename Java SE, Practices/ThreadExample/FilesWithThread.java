/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileswiththread;
/**
 *
 * @author Norbi
 * */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FilesWithThread {
   

    public static void rowCounter(int createdFiles, List<Integer> list) throws FileNotFoundException, InterruptedException {
        BufferedReader inputStream;
        for (int i = 1; i <= createdFiles; i++) {
            inputStream = new BufferedReader(new FileReader("C:\\Workspace\\" + i + ".txt"));
            FileThread fileThread = new FileThread(inputStream, "threadingtheneedle" + i + ".txt", list);
            Thread thread = new Thread(fileThread);
            thread.start();
        }
    }

    public static void createFiles(int createdFiles) throws IOException {
        PrintWriter outputStream = null;
        for (int i = 1; i <= createdFiles; i++) {
            try {
                outputStream = new PrintWriter(new FileWriter("C:\\Workspace\\" + i + ".txt"));
                int rowNumber = 100000 + (int) (Math.random() * 100000);
                for (int j = 0; j < rowNumber; j++) {
               
    
                    outputStream.println("threadingtheneedle" + j);

                }
            } finally {
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        
        
        int fileNumber = extra.Console.readInt("Enter the number of files you want to create: ");
        List<Integer> list = new ArrayList<>();

        createFiles(fileNumber);

        rowCounter(fileNumber, list);
       
        while(Thread.activeCount()>1){
            
        }
        
        int sumOfRows = list.stream().mapToInt(e -> e).sum();
        System.out.println("You created " + fileNumber + " files");
                System.out.println("Sum of rows : " + sumOfRows );
               
    }
    
    
}

