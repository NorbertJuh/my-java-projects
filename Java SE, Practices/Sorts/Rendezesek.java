/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rendezesek;

/**
 *
 * @author Norbi
 */
public class Rendezesek {

    final static int N = 20;

    static int[] array = new int[N + 1];

    static void arrayload() {
        for (int i = 1; i <= N; i++) {
            array[i] = (int) (Math.random() * 90 + 10);
        }
        
    }

    static void arrayList(String message) {
        System.out.println(message);
        for (int i = 1; i < N; i++) {
            System.out.print(array[i] + ", ");

        }
    }

    static void arraySimpleSort() {
        System.out.println("");
        int swap;
        for (int i = 1; i <= N - 1; i++) {
            for (int j = i + 1; j <= N; j++) {
                if (array[i] > array[j]) {
                    swap = array[i];
                    array[i] = array[j];
                    array[j] = swap;
                }
            }
        }

    }

    static void minimumSort() {
        int swap, minPoint;
        for (int i = 1; i <= N - 1; i++) {
            minPoint = i;
            for (int j = i + 1; j <= N; j++) {
                if (array[minPoint] > array[j]) {
                    minPoint = j;
                }
            }
            swap = array[i];
            array[i] = array[minPoint];
            array[minPoint] = swap;

        }
    }

    static void bubbleSort() {
        int swap;
        for (int i = N; i >= 2; i--) {
            for (int j = 1; j <= i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swap;
                }
            }

        }
    }
    
    static void fastSort(int min, int max) {
        
        int i = min, j = max, swap;
        int separator = array[(min+max)/2];
        while(i<=j){
            while(array[i]<separator)
                i++;
            while(array[j]>separator)
                j--;
            if(i<j){
                swap=array[i];
                array[i]=array[j];
                array[j]=swap;
            }
            if(i<=j){
                i++;
                j--;
            }
        }
        if(min<j)
            fastSort(min,j);
        if (i<max)
            fastSort(i,max);
        
    }

    public static void main(String[] args) {
        arrayload();
        arrayList("Array before SimpleSort: ");
        arraySimpleSort();
        arrayList("Array after SimpleSort: ");
        arrayload();
        bubbleSort();
        arrayList("Array after bubblesort");
        
        
        
        
        
        long startTime = System.nanoTime();
        
        for (int i = 1; i < 100000; i++) {
            arrayload();
            minimumSort();
            
        }
        long endTime = System.nanoTime();
        arrayList("Array after Minimumsort: ");
        System.out.println("Time in microsec: " + (endTime-startTime)/1000);
        
        
        long startTime2 = System.nanoTime();
        
        for (int i = 1; i < 100000; i++) {
            arrayload();
            fastSort(1, N);
            
        }
         long endTime2 = System.nanoTime();
        arrayList("Array after fastsort");
        
        System.out.println("Time in microsec: " + (endTime2-startTime2)/1000);
        
    }

}
