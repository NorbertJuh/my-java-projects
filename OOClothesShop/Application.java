/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop;

import shop.model.ExternalOrders;
import shop.model.ProductFactory;
import shop.repository.ProductRepository;
import shop.view.View;

/**
 *
 * @author Norbi
 */
public class Application {


    public static void main(String[] args) {

        ProductFactory productFactory = new ProductFactory();
        ProductRepository productRepository = new ProductRepository();
        ExternalOrders externalOrders = new ExternalOrders();
        View productView = new View(productRepository.getProductRepository(),externalOrders.getOrders());
        
        productRepository.addProduct(productFactory.getInstance("SIMPLEJACKET",10000));
        productRepository.addProduct(productFactory.getInstance("HAT",4000));
        productRepository.addProduct(productFactory.getInstance("BELT",1500));
        productRepository.addProduct(productFactory.getInstance("SIMPLELINGERIE",3000));
        productRepository.addProduct(productFactory.getInstance("STREETSHOE",17500));
        productRepository.addProduct(productFactory.getInstance("HIKINGSHOE",18000));
        productRepository.addProduct(productFactory.getInstance("UNISEXCOAT",20000));
        productRepository.addProduct(productFactory.getInstance("RAINCOAT",8000));
        productRepository.addProduct(productFactory.getInstance("OTHER",2000));
        productRepository.addProduct(productFactory.getInstance("JEANSJACKET",15000));
               
        System.out.println("Welcome to Clothes Shop! \n");
        productView.printProducts();

        productRepository.addProduct(productFactory.getInstance("TWOPIECELINGERIE",7000));

        System.out.println("Add an item: ");
        
        productView.printProducts();
        
        productRepository.delete(4);

        System.out.println("Remove the 4th id item");
        productView.printProducts();

               
        
        String path = "C:\\Workspace\\outside.csv";
        externalOrders.readExternalOrder(path,productRepository,productFactory);

                
        System.out.println("The external order data: ");
        productView.printExOrder();
        
        System.out.println("Items list after the external order: \n");
        productView.printProducts();
    }

}
