/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import shop.model.Product;

public class ProductRepository {

    private static List<Product> productRepository = new ArrayList<>();

    public void addProduct(Product product) {
        productRepository.add(product);
    }

    public void delete(int id) {

        Optional<Product> optionalProduct = findById(id);

        if (optionalProduct.isPresent()) {
            productRepository.remove(optionalProduct.get());
        } else {
            System.out.println("Item not found");
        }

    }

    public Optional<Product> findById(int id) {

        return productRepository.stream().filter(e -> e.getId() == id).findFirst();

    }

    public List<Product> getProductRepository() {
        return productRepository;
    }

    @Override
    public String toString() {
        return "ProductRepository{" + "productRepository=" + productRepository + '}';
    }

}
