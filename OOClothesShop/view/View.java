package shop.view;

import java.util.List;
import shop.model.Order;
import shop.model.Product;

public class View {

    List<Product> productList;
    List<Order> orderList;

    public View(List<Product> productList, List<Order> orderList) {
        this.productList = productList;
        this.orderList = orderList;
    }

    public void printProducts() {
        System.out.println("List of products: ");
        productList.stream().forEach(System.out::println);
        System.out.println("---------------------------------------------\n");     
    }

    public void printExOrder() {    
        orderList.stream().forEach(System.out::println);
        System.out.println("---------------------------------------------");
        System.out.println("");
    }

}
