/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.ProductTypes.OtherType;
import shop.model.Product;


public class Other extends Product {

    OtherType type;

    public Other(OtherType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public OtherType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Other{ id " + id + " type= " + type + " price= " + price + '}';
    }

}
