/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.ProductTypes.LingerieType;
import shop.model.Product;


public class Lingerie extends Product {

    LingerieType type;

    public Lingerie(LingerieType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public LingerieType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Lingerie{ id " + id + " type= " + type + " price= " + price + '}';
    }
}
