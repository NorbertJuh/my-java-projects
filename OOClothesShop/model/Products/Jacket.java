/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.ProductTypes.JacketType;
import shop.model.Product;


public class Jacket extends Product {

    JacketType type;

    public Jacket(JacketType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public JacketType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Jacket{ id " + id + " type= " + type + " price= " + price + '}';
    }
}
