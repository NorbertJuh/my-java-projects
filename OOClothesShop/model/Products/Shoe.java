/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.Product;
import shop.model.ProductTypes.ShoeType;


public class Shoe extends Product {

    ShoeType type;

    public Shoe(ShoeType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public ShoeType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Shoe{ id " + id + " type= " + type + " price= " + price + '}';
    }
}
