/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.ProductTypes.AccessoryType;
import shop.model.Product;


public class Accessory extends Product {

    AccessoryType type;

    public Accessory(AccessoryType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public AccessoryType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Accessory{ id " + id + " type= " + type + " price= " + price + '}';
    }
    

}
