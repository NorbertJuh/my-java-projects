/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.Products;

import shop.model.ProductTypes.CoatType;
import shop.model.Product;


public class Coat extends Product {

    CoatType type;

    public Coat(CoatType type, int price) {
        this.type = type;
        this.price = price;
        this.id = Product.getNextId();
        setNextId();
    }

    public CoatType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Coat{ id " + id + " type= " + type + " price= " + price + '}';
    }

    
    
}

