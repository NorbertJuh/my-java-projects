/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model;

public class Order<T1, T2, T3, T4, T5> {

    private T1 date;
    private T2 price;
    private T3 performed;
    private T4 pieces;
    private T5 productType;

    public Order(T1 date, T2 price, T3 performed, T4 pieces, T5 productType) {
        this.date = date;
        this.price = price;
        this.performed = performed;
        this.pieces = pieces;
        this.productType = productType;
    }

 
    @Override
    public String toString() {
        return "Order{" + "date=" + date + ", price=" + price + ", performed=" + performed + ", pieces=" + pieces + ", productType=" + productType + '}';
    }
    
    
}
