/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model.ProductTypes;

/**
 *
 * @author Norbi
 */
public enum ShoeType {
    WOMANSHOE,
    MANSHOE,
    WATERPROOFSHOE,
    NOTWATERPROOFSHOE,
    STREETSHOE,
    HIKINGSHOE,
    SPORTSHOE

}
