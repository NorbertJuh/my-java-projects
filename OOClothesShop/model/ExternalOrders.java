/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model;


import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import shop.repository.ProductRepository;

public class ExternalOrders {

    private List<Order> orders = new ArrayList<>();
 
    public void readExternalOrder(String path,ProductRepository productRepository, ProductFactory productFactory) {
                               
        if (new File(path).isFile()) {

            try (FileReader fr = new FileReader(path);
                    CSVReader csv = new CSVReader(fr)) {

                String[] row;
                while ((row = csv.readNext()) != null) {

                    String date = row[0];
                    int price = Integer.parseInt(row[1]);
                    boolean performed = Boolean.valueOf(row[2].toLowerCase());
                    int count = Integer.parseInt(row[3]);
                    String type = row[4];
                    
                //    price = (int) orders.stream().count();
                 //   count = (int) orders.stream().count();

                    if (performed) {

                        for (int i = 0; i < count; i++) {
                            productRepository.addProduct(productFactory.getInstance(type, price));
                        }
                        orders.add(new Order(date, price, performed, count, type));

                    } else {

                        try (FileWriter fw = new FileWriter("C:\\Workspace\\outside.csv", true);
                                CSVWriter csvWriter = new CSVWriter(fw)) {
                            csvWriter.writeNext(row);
                        }

                    }
                }
            } catch (IOException ex) {
                System.out.println("File opening failed");
            }
        }
    }

    public List<Order> getOrders() {
        return orders;
    }

}
