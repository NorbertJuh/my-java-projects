/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model;

import shop.model.ProductTypes.ShoeType;
import shop.model.ProductTypes.OtherType;
import shop.model.ProductTypes.LingerieType;
import shop.model.ProductTypes.JacketType;
import shop.model.ProductTypes.CoatType;
import shop.model.ProductTypes.AccessoryType;
import shop.model.Products.Shoe;
import shop.model.Products.Other;
import shop.model.Products.Lingerie;
import shop.model.Products.Jacket;
import shop.model.Products.Coat;
import shop.model.Products.Accessory;

public class ProductFactory {

    public Product getInstance(String productType, int price) {

        switch (productType.toUpperCase()) {

            case ("HAT"):
                return new Accessory(AccessoryType.HAT, price);
                
            case ("BELT"):
                return new Accessory(AccessoryType.BELT, price);
                
            case ("WATCH"):
                return new Accessory(AccessoryType.WATCH, price);

            case ("SIMPLECOAT"):
                return new Coat(CoatType.SIMPLECOAT, price);

            case ("WOMANCOAT"):
                return new Coat(CoatType.WOMANCOAT, price);

            case ("MANCOAT"):
                return new Coat(CoatType.MANCOAT, price);

            case ("UNISEXCOAT"):
                return new Coat(CoatType.UNISEXCOAT, price);

            case ("RAINCOAT"):
                return new Coat(CoatType.RAINCOAT, price);

            case ("WINTERCOAT"):
                return new Coat(CoatType.WINTERCOAT, price);

            case ("SIMPLEJACKET"):
                return new Jacket(JacketType.SIMPLEJACKET, price);

            case ("LEATHERJACKET"):
                return new Jacket(JacketType.LEATHERJACKET, price);

            case ("JEANSJACKET"):
                return new Jacket(JacketType.JEANSJACKET, price);

            case ("SIMPLELINGERIE"):
                return new Lingerie(LingerieType.SIMPLELINGERIE, price);

            case ("TWOPIECELINGERIE"):
                return new Lingerie(LingerieType.TWOPIECELINGERIE, price);

            case ("ONEPIECELINGERIE"):
                return new Lingerie(LingerieType.ONEPIECELINGERIE, price);

            case ("OTHER"):
                return new Other(OtherType.OTHER, price);

            case ("MANSHOE"):
                return new Shoe(ShoeType.MANSHOE, price);

            case ("WOMANSHOE"):
                return new Shoe(ShoeType.WOMANSHOE, price);

            case ("WATERPROOFSHOE"):
                return new Shoe(ShoeType.WATERPROOFSHOE, price);

            case ("NOTWATERPROOFSHOE"):
                return new Shoe(ShoeType.NOTWATERPROOFSHOE, price);

            case ("STREETSHOE"):
                return new Shoe(ShoeType.STREETSHOE, price);

            case ("HIKINGSHOE"):
                return new Shoe(ShoeType.HIKINGSHOE, price);

            case ("SPORTSHOE"):
                return new Shoe(ShoeType.SPORTSHOE, price);

            default:
                throw new IllegalArgumentException();
        }
    }

}
