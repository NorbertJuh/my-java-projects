/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.model;


public abstract class Product implements Comparable<Product> {

    private static int nextId = 1;
    protected int id;
    protected int price;

    public static int getNextId() {
        return nextId;
    }

    public int getId() {
        return id;
    }     

    protected static void setNextId() {
        Product.nextId++;
    }
    
    
    
    @Override
    public int compareTo(Product product) {
        if (this.price > product.price) {
            return 1;
        } else if (this.price == product.price) {
            return 0;
        } else {
            return -1;
        }
    }

}
