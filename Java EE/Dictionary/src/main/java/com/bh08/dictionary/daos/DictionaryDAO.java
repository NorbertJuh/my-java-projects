package com.bh08.dictionary.daos;

import javax.persistence.*;

import com.bh08.dictionary.model.EngHunDictionary;

public class DictionaryDAO implements DAO<EngHunDictionary>{

	private static DictionaryDAO instance;
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Dictionary");

	private DictionaryDAO() {
	}
	
	public static DictionaryDAO getInstance() {
		if (instance == null) {
			instance = new DictionaryDAO();
		}
		return instance;
	}

	@Override
	public EngHunDictionary findEngWord(String engWord) {
		EntityManager em = emf.createEntityManager();
		TypedQuery<EngHunDictionary> q = em.createQuery("SELECT d FROM Norbi.EngHunDictionary d WHERE d.engWord = :engWord", EngHunDictionary.class);
		q.setParameter("engWord", engWord);
		try {
			EngHunDictionary result = q.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public EngHunDictionary save(EngHunDictionary obj) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		em.persist(obj);
		
		tx.commit();
		
		em.close();
		return obj;
		
	}

	public EngHunDictionary findHunWord(String hunWord) {
		EntityManager em = emf.createEntityManager();
		TypedQuery<EngHunDictionary> q = em.createQuery("SELECT d FROM Norbi.EngHunDictionary d WHERE d.hunWord = :hunWord", EngHunDictionary.class);
		q.setParameter("hunWord", hunWord);
		try {
			EngHunDictionary result = q.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
	}
}
