package com.bh08.dictionary.daos;

public interface DAO<T> {

	T findEngWord(String word);
	
	T save(T obj);
}
