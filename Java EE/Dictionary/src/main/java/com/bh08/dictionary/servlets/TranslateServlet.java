package com.bh08.dictionary.servlets;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.dictionary.model.EngHunDictionary;
import com.bh08.dictionary.service.DictionaryService;

/**
 * Servlet implementation class TranslateServlet
 */
@WebServlet("/translate")
public class TranslateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DictionaryService service = new DictionaryService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TranslateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("DoGetTranslate");
		String word = (String) request.getParameter("word");
		String radioButton = (String) request.getParameter("radio");

		EngHunDictionary translatedWord = null;

		switch (radioButton) {
			case "EngHun":
				translatedWord = service.findEngWord(word);
				break;
			case "HunEng":
				translatedWord = service.findHunWord(word);
				break;
		}

		request.setAttribute("translatedWord", translatedWord);

		request.getRequestDispatcher("index.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
