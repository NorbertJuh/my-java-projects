package com.bh08.dictionary.service;

import com.bh08.dictionary.daos.DictionaryDAO;
import com.bh08.dictionary.model.EngHunDictionary;

public class DictionaryService {

	private DictionaryDAO dao = DictionaryDAO.getInstance();
	
	public EngHunDictionary findEngWord(String word) {
		return dao.findEngWord(word);
	}
	
	public EngHunDictionary save(EngHunDictionary obj) {
		return dao.save(obj);
	}

	public EngHunDictionary findHunWord(String word) {
		return  dao.findHunWord(word);
	}
	
}
