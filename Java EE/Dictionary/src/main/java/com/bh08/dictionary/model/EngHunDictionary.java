package com.bh08.dictionary.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class EngHunDictionary {

	@Id
	@GeneratedValue(generator = "mySeqGenEngHun")
	@SequenceGenerator(name = "mySeqGenEngHun", sequenceName = "seqEngHun01", initialValue = 1)
	private long id;

	@Column(unique = true, nullable = false)
	private String engWord;
	
	@Column(unique = true, nullable = false)
	private String hunWord;
	
}
