<%@page import="com.bh08.dictionary.model.EngHunDictionary"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dictionary</title>
</head>
<body>
	<form method="GET" action="/Dictionary/translate">
		<br><br><br><br><br><br><br>
		<input type='radio' name='radio' value='EngHun' checked="checked" />Angol-Magyar
		<input type='radio' name='radio' value='HunEng' />Magyar-Angol<br />

		<label for="char">Keresett szó: </label> 
		<input type="text" name="word" id="word" /><br /> 
		<input type="submit" value="Fordít" /><br />
		<br />

		<%
			EngHunDictionary engHunDictionaries = (EngHunDictionary) request.getAttribute("translatedWord");

			String translatedWord = null;

			String radioButton = request.getParameter("radio");

			if (engHunDictionaries != null) {

				switch (radioButton) {
				case "EngHun":
					out.println("<b>Keresett szó: </b>" + engHunDictionaries.getEngWord() + "<br/>");
					out.println("<b>Fordítás: </b>" + engHunDictionaries.getHunWord() + "<br/>");
					break;
				case "HunEng":
					out.println("<b>Keresett szó: </b>" + engHunDictionaries.getHunWord() + "<br/>");
					out.println("<b>Fordítás: </b>" + engHunDictionaries.getEngWord() + "<br/>");
					break;
				}

			} else {
				out.println("<b>A keresett szó nem található.</b>");
			}
		%>


	</form>

</body>
</html>
