create table konyv_peldany(konyv_azonosito varchar(100) primary key not null, isbn varchar(100));
create table konyv(isbn varchar(100) primary key not null, cim varchar(200));
create table szerzo(szerzo_id varchar(100) primary key not null, szerzo_nev varchar(200));
create table konyvszerzo(isbn varchar(100), szerzo_id varchar(100), CONSTRAINT szerzo_cnt primary key (isbn, szerzo_id));
create table olvaso(olvaso_azonosito varchar(100) primary key not null, nev varchar(200), cim varchar(200));
create table kolcsonoz(olvaso_azonosito varchar(100), konyv_azonosito varchar(100), kiv_date date, vissz_date date, kolcs_azonosito varchar(100) primary key not null);

INSERT INTO konyv_peldany 
VALUES ('Egri Csillagok', '9789631313338');
VALUES ('Star Wars', '4523556775314');
VALUES ('P�l utcai Fi�k', '3423431313338');
VALUES ('Java Programoz�s', '1111313133385')

select * from konyv_peldany;