/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryapp.view;

/**
 *
 * @author Norbi
 */
   

import libraryapp.controller.Controller;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Csaba
 */
public class View {

    private Controller controller;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    private JFrame MenuWindow;

    private JFrame BooksMenuWindow;
    private JFrame ListBooksWindow;
    private JFrame AddNewBookWindow;
    private JFrame AddNewBookTypeWindow;
    private JFrame AddNewAuthorWindow;

    private JFrame ListReadersWindow;
    private JFrame AddNewReaderWindow;
    private JFrame ReadersMenuWindow;

    private JFrame BorrowWindow;
    private JFrame BorrowBooksWindow;
    private JFrame BooksBackWindow;

    private JFrame ELibraryWindow;

//MENÜ
    public void showMenu() {
        MenuWindow = new JFrame();

        JButton btBooks = new JButton("Books");
        JButton btReaders = new JButton("Readers");
        JButton btBorrow = new JButton("Borrow");
        JButton btELibrary = new JButton("E-library");
        JButton btExit = new JButton("Quit");

        MenuWindow.setSize(500, 500);
        MenuWindow.setLocationRelativeTo(null);
        MenuWindow.setTitle("Library App");
        MenuWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        MenuWindow.setResizable(false);

        JPanel pnMenu = new JPanel(new GridLayout(5, 1));

        pnMenu.add(btBooks);
        pnMenu.add(btReaders);
        pnMenu.add(btBorrow);
        pnMenu.add(btELibrary);
        pnMenu.add(btExit);

        MenuWindow.add(pnMenu);

        MenuWindow.setVisible(true);

        btBooks.addActionListener(ae -> {
            showBooksWindow();
            MenuWindow.dispose();
        }
        );

        btReaders.addActionListener(ae -> {
            showReadersWindow();
            MenuWindow.dispose();
        }
        );

        btBorrow.addActionListener(ae -> {
            showBorrowWindow();
            MenuWindow.dispose();
        }
        );

        btELibrary.addActionListener(ae -> {
           // showELibraryWindow();
            MenuWindow.dispose();
        }
        );

        btExit.addActionListener(ae -> {
            Object[] opcioTomb = {"Yes", "No"};
            int opcio = JOptionPane.showOptionDialog(
                    MenuWindow,
                    "Do you want to quit ?",
                    "Quit",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    opcioTomb,
                    opcioTomb[1]);
            if (opcio == JOptionPane.YES_OPTION) {
                controller.closeTheProgram();
            }
        }
        );

    }

//KÖNYVEK    
    public void showBooksWindow() {
        BooksMenuWindow = new JFrame();

        JButton btListBooks = new JButton("Avaiable books");
        JButton btAddNewBook = new JButton("New bookobject");
        JButton btAddNewBookType = new JButton("Add a book");
        JButton btAddNewAuthor = new JButton("New Author");
        JButton btBack = new JButton("Back");

        BooksMenuWindow.setSize(300, 200);
        BooksMenuWindow.setLocationRelativeTo(null);
        BooksMenuWindow.setTitle("Books");
        BooksMenuWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        BooksMenuWindow.setResizable(false);

        JPanel pnBooksMenu = new JPanel(new GridLayout(5, 1));
        pnBooksMenu.add(btListBooks);
        pnBooksMenu.add(btAddNewBook);
        pnBooksMenu.add(btAddNewBookType);
        pnBooksMenu.add(btAddNewAuthor);
        pnBooksMenu.add(btBack);

        BooksMenuWindow.add(pnBooksMenu);

        BooksMenuWindow.setVisible(true);

        btListBooks.addActionListener(ae -> {
            listBooks();
            BooksMenuWindow.dispose();
        }
        );

        btAddNewBook.addActionListener(ae -> {
            showAddNewBook();
            BooksMenuWindow.dispose();
        }
        );

        btAddNewBookType.addActionListener(ae -> {
            showAddNewBookType();
            BooksMenuWindow.dispose();
        }
        );

        btAddNewAuthor.addActionListener(ae -> {
            showAddNewAuthorWindow();
            BooksMenuWindow.dispose();
        }
        );

        btBack.addActionListener(ae -> {
            showMenu();
            BooksMenuWindow.dispose();
        }
        );
    }
//Könyvek litázása

    public void listBooks() {
        ListBooksWindow = new JFrame();

        JLabel lbAvailable = new JLabel("Borrowable books");
        JList lsAvailable = new JList(controller.listBooks("available"));
        JLabel lbBorrowed = new JLabel("Borrowed books");
        JList lsBorrowed = new JList(controller.listBooks("borrowed"));

        JLabel lbSearchWord = new JLabel("Search: ");
        JTextField tfSearchWord = new JTextField(20);
        JButton btSearchByAuthor = new JButton("Author search");
        JButton btSearchByTitle = new JButton("Title search");
        JButton btBack = new JButton("Back");

        ListBooksWindow.setSize(1000, 800);
        ListBooksWindow.setLocationRelativeTo(null);
        ListBooksWindow.setTitle("List books");
        ListBooksWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        ListBooksWindow.setResizable(false);
        ListBooksWindow.setLayout(new BorderLayout());

        lsAvailable.setEnabled(false);
        JScrollPane spListAvailableBooks = new JScrollPane(lsAvailable);
        lsBorrowed.setEnabled(false);
        JScrollPane spBorrowedBooks = new JScrollPane(lsBorrowed);

        JPanel pnAvailable = new JPanel(new BorderLayout());
        pnAvailable.add(lbAvailable, BorderLayout.NORTH);
        pnAvailable.add(spListAvailableBooks, BorderLayout.CENTER);

        JPanel pnBorrowed = new JPanel(new BorderLayout());
        pnBorrowed.add(lbBorrowed, BorderLayout.NORTH);
        pnBorrowed.add(spBorrowedBooks, BorderLayout.CENTER);

        JPanel pnLists = new JPanel(new GridLayout(1, 2));
        pnLists.add(pnAvailable);
        pnLists.add(pnBorrowed);

        ListBooksWindow.add(pnLists, BorderLayout.CENTER);

        JPanel pnSearch = new JPanel(new GridLayout(2, 1));
        JPanel pnSearchField = new JPanel();
        pnSearchField.add(lbSearchWord);
        pnSearchField.add(tfSearchWord);
        pnSearchField.add(btSearchByAuthor);
        pnSearchField.add(btSearchByTitle);
        pnSearch.add(pnSearchField);
        JPanel pnBack = new JPanel();
        pnBack.add(btBack);
        pnSearch.add(pnBack);

        ListBooksWindow.add(pnSearch, BorderLayout.SOUTH);

        ListBooksWindow.setVisible(true);

        btSearchByAuthor.addActionListener(ae -> {
            ListBooksWindow.dispose();
            JFrame ListBooksSearchedByName = new JFrame();

            JLabel lbSearchAvailable = new JLabel("Borrowable books");
            JLabel lbSearchBorrowed = new JLabel("Borrowed books");
            JList ListBooksSearchedByNameAvailable = new JList(controller.searchBooks(tfSearchWord.getText(), "author", "available"));
            JList ListBooksSearchedByNameBorrowed = new JList(controller.searchBooks(tfSearchWord.getText(), "author", "borrowed"));
            JButton btBack2 = new JButton("Back");

            ListBooksSearchedByName.setSize(1000, 800);
            ListBooksSearchedByName.setLocationRelativeTo(null);
            ListBooksSearchedByName.setTitle("Search result");
            ListBooksSearchedByName.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            ListBooksSearchedByName.setResizable(false);
            ListBooksSearchedByName.setLayout(new BorderLayout());

            ListBooksSearchedByNameAvailable.setEnabled(false);
            ListBooksSearchedByNameBorrowed.setEnabled(false);
            JScrollPane spListReadersSeaechedByNameAvailable = new JScrollPane(ListBooksSearchedByNameAvailable);
            JScrollPane spListReadersSeaechedByNameBorrowed = new JScrollPane(ListBooksSearchedByNameBorrowed);

            JPanel pnSearchAuthorAvailable = new JPanel(new BorderLayout());
            pnSearchAuthorAvailable.add(lbSearchAvailable, BorderLayout.NORTH);
            pnSearchAuthorAvailable.add(spListReadersSeaechedByNameAvailable, BorderLayout.CENTER);

            JPanel pnSearchAuthorBorrowed = new JPanel(new BorderLayout());
            pnSearchAuthorBorrowed.add(lbSearchBorrowed, BorderLayout.NORTH);
            pnSearchAuthorBorrowed.add(spListReadersSeaechedByNameBorrowed, BorderLayout.CENTER);

            JPanel pnAuthorlists = new JPanel(new GridLayout(1, 2));
            pnAuthorlists.add(pnSearchAuthorAvailable);
            pnAuthorlists.add(pnSearchAuthorBorrowed);

            ListBooksSearchedByName.add(pnAuthorlists, BorderLayout.CENTER);

            JPanel pnBack2 = new JPanel();
            pnBack2.add(btBack2);
            ListBooksSearchedByName.add(pnBack2, BorderLayout.SOUTH);

            ListBooksSearchedByName.setVisible(true);

            btBack2.addActionListener(ae2 -> {
                listBooks();
                ListBooksSearchedByName.dispose();
            }
            );
        }
        );

        btSearchByTitle.addActionListener(ae -> {
            ListBooksWindow.dispose();
            JFrame ListBooksSearchedByTitle = new JFrame();

            JLabel lbSearchAvailable = new JLabel("Borrowable books");
            JLabel lbSearchBorrowed = new JLabel("Borrowed books");
            JList ListBooksSearchedByNameAvailable = new JList(controller.searchBooks(tfSearchWord.getText(), "title", "available"));
            JList ListBooksSearchedByNameBorrowed = new JList(controller.searchBooks(tfSearchWord.getText(), "title", "borrowed"));
            JButton btBack2 = new JButton("Back");

            ListBooksSearchedByTitle.setSize(1300, 600);
            ListBooksSearchedByTitle.setLocationRelativeTo(null);
            ListBooksSearchedByTitle.setTitle("Search result");
            ListBooksSearchedByTitle.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            ListBooksSearchedByTitle.setResizable(false);
            ListBooksSearchedByTitle.setLayout(new BorderLayout());

            ListBooksSearchedByNameAvailable.setEnabled(false);
            ListBooksSearchedByNameBorrowed.setEnabled(false);
            JScrollPane spListReadersSeaechedByNameAvailable = new JScrollPane(ListBooksSearchedByNameAvailable);
            JScrollPane spListReadersSeaechedByNameBorrowed = new JScrollPane(ListBooksSearchedByNameBorrowed);

            JPanel pnSearchAuthorAvailable = new JPanel(new BorderLayout());
            pnSearchAuthorAvailable.add(lbSearchAvailable, BorderLayout.NORTH);
            pnSearchAuthorAvailable.add(spListReadersSeaechedByNameAvailable, BorderLayout.CENTER);

            JPanel pnSearchAuthorBorrowed = new JPanel(new BorderLayout());
            pnSearchAuthorBorrowed.add(lbSearchBorrowed, BorderLayout.NORTH);
            pnSearchAuthorBorrowed.add(spListReadersSeaechedByNameBorrowed, BorderLayout.CENTER);

            JPanel pnAuthorlists = new JPanel(new GridLayout(1, 2));
            pnAuthorlists.add(pnSearchAuthorAvailable);
            pnAuthorlists.add(pnSearchAuthorBorrowed);

            ListBooksSearchedByTitle.add(pnAuthorlists, BorderLayout.CENTER);

            JPanel pnBack2 = new JPanel();
            pnBack2.add(btBack2);
            ListBooksSearchedByTitle.add(pnBack2, BorderLayout.SOUTH);

            ListBooksSearchedByTitle.setVisible(true);

            btBack2.addActionListener(ae2 -> {
                listBooks();
                ListBooksSearchedByTitle.dispose();
            }
            );
        }
        );

        btBack.addActionListener(ae -> {
            showBooksWindow();
            ListBooksWindow.dispose();
        }
        );
    }

//ÚJ könyvpéldány hozzáadása
    public void showAddNewBook() {
        AddNewBookWindow = new JFrame();

        JLabel lbBookID = new JLabel("Book id");
        JTextField tfBookID = new JTextField(controller.generateIDNumber(), 10);
        JLabel lbISBN = new JLabel("ISBN: ");
        JList lsISBN = new JList(controller.listISBN());
        lsISBN.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JButton btAdd = new JButton("Add");
        JButton btBack = new JButton("Back");

        AddNewBookWindow.setSize(500, 500);
        AddNewBookWindow.setLocationRelativeTo(null);
        AddNewBookWindow.setTitle("New book object");
        AddNewBookWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AddNewBookWindow.setResizable(false);
        AddNewBookWindow.setLayout(new BorderLayout());

        JScrollPane spListISBN = new JScrollPane(lsISBN);

        JPanel pnBookID = new JPanel();
        pnBookID.add(lbBookID);
        pnBookID.add(tfBookID);
        AddNewBookWindow.add(pnBookID, BorderLayout.NORTH);

        JPanel pnISBN = new JPanel();
        pnISBN.add(lbISBN);
        pnISBN.add(spListISBN);
        AddNewBookWindow.add(pnISBN, BorderLayout.CENTER);

        JPanel pnButtons = new JPanel(new GridLayout(2, 1));
        pnButtons.add(btAdd);
        pnButtons.add(btBack);
        AddNewBookWindow.add(pnButtons, BorderLayout.SOUTH);

        AddNewBookWindow.setVisible(true);

        btAdd.addActionListener(ae -> {
            String BookID = tfBookID.getText();
            Object ISBN = lsISBN.getSelectedValue();

            controller.addBookToKonyvPeldanyTable(BookID, ISBN);

            AddNewBookWindow.dispose();
            showBooksWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showBooksWindow();
            AddNewBookWindow.dispose();
        }
        );

    }

//ÚJ Könyv felvétele
    public void showAddNewBookType() {
        AddNewBookTypeWindow = new JFrame();

        JLabel lbISBN = new JLabel("ISBN: ISBN-");
        JTextField tfISBN = new JTextField(controller.generateIDNumber(), 10);
        JLabel lbTitle = new JLabel("Title: ");
        JTextField tfTitle = new JTextField(20);
        JLabel lbAuthorID = new JLabel("Author ID: ");
        JList lsAuthorID = new JList(controller.listAuthorID());
        lsAuthorID.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JButton btAdd = new JButton("Add");
        JButton btBack = new JButton("Back");

        AddNewBookTypeWindow.setSize(600, 600);
        AddNewBookTypeWindow.setLocationRelativeTo(null);
        AddNewBookTypeWindow.setTitle("Add book");
        AddNewBookTypeWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AddNewBookTypeWindow.setResizable(false);
        AddNewBookTypeWindow.setLayout(new BorderLayout());

        JScrollPane spListAuthorID = new JScrollPane(lsAuthorID);

        JPanel pnISBN = new JPanel();
        pnISBN.add(lbISBN);
        pnISBN.add(tfISBN);

        JPanel pnTitle = new JPanel();
        pnTitle.add(lbTitle);
        pnTitle.add(tfTitle);

        JPanel pnISBNTitle = new JPanel(new GridLayout(2, 1));
        pnISBNTitle.add(pnISBN);
        pnISBNTitle.add(pnTitle);

        JPanel pnAuthorID = new JPanel();
        pnAuthorID.add(lbAuthorID);
        pnAuthorID.add(spListAuthorID);

        JPanel pnButtons = new JPanel(new GridLayout(2, 1));
        pnButtons.add(btAdd);
        pnButtons.add(btBack);

        AddNewBookTypeWindow.add(pnISBNTitle, BorderLayout.NORTH);
        AddNewBookTypeWindow.add(pnAuthorID, BorderLayout.CENTER);
        AddNewBookTypeWindow.add(pnButtons, BorderLayout.SOUTH);

        AddNewBookTypeWindow.setVisible(true);

        btAdd.addActionListener(ae -> {
            String ISBN = tfISBN.getText();
            String title = tfTitle.getText();
            Object[] authors = lsAuthorID.getSelectedValues();

            controller.addBookToKonyvTable(ISBN, title);
            controller.addBookToKonyszerzoTable(ISBN, authors);

            AddNewBookTypeWindow.dispose();
            showBooksWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showBooksWindow();
            AddNewBookTypeWindow.dispose();
        }
        );
    }

//Új szerző felvétele
    public void showAddNewAuthorWindow() {
        AddNewAuthorWindow = new JFrame();

        JLabel lbID = new JLabel("Author ID: ");
        JTextField tfID = new JTextField(controller.generateIDNumber(), 10);
        JLabel lbName = new JLabel("Name: ");
        JTextField tfName = new JTextField(20);
        JButton btAdd = new JButton("Add");
        JButton btBack = new JButton("Back");

        AddNewAuthorWindow.setSize(300, 200);
        AddNewAuthorWindow.setLocationRelativeTo(null);
        AddNewAuthorWindow.setTitle("New author");
        AddNewAuthorWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AddNewAuthorWindow.setResizable(false);
        AddNewAuthorWindow.setLayout(new GridLayout(4, 1));

        JPanel pnID = new JPanel();
        pnID.add(lbID);
        pnID.add(tfID);

        JPanel pnName = new JPanel();
        pnName.add(lbName);
        pnName.add(tfName);

        AddNewAuthorWindow.add(pnID);
        AddNewAuthorWindow.add(pnName);
        AddNewAuthorWindow.add(btAdd);
        AddNewAuthorWindow.add(btBack);

        AddNewAuthorWindow.setVisible(true);

        btAdd.addActionListener(ae -> {
            String id = tfID.getText();
            String name = tfName.getText();
            controller.addNewAuthor(id, name);
            AddNewAuthorWindow.dispose();
            showBooksWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showBooksWindow();
            AddNewAuthorWindow.dispose();
        }
        );
    }

//OLVASÓK    
    public void showReadersWindow() {
        ReadersMenuWindow = new JFrame();

        JButton btListReaders = new JButton("List readers");
        JButton btAddNewReader = new JButton("Add reader");
        JButton btBack = new JButton("Back");

        ReadersMenuWindow.setSize(600, 600);
        ReadersMenuWindow.setLocationRelativeTo(null);
        ReadersMenuWindow.setTitle("Readers");
        ReadersMenuWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        ReadersMenuWindow.setResizable(false);

        JPanel pnReadersMenu = new JPanel(new GridLayout(3, 1));
        pnReadersMenu.add(btListReaders);
        pnReadersMenu.add(btAddNewReader);
        pnReadersMenu.add(btBack);

        ReadersMenuWindow.add(pnReadersMenu);

        ReadersMenuWindow.setVisible(true);

        btListReaders.addActionListener(ae -> {
            showListReadersWindow();
            ReadersMenuWindow.dispose();
        }
        );

        btAddNewReader.addActionListener(ae -> {
            showAddNewReaderWindow();
            ReadersMenuWindow.dispose();
        }
        );

        btBack.addActionListener(ae -> {
            showMenu();
            ReadersMenuWindow.dispose();
        }
        );

    }

//Olvasók listázása    
    public void showListReadersWindow() {
        ListReadersWindow = new JFrame();

        JList lsReaders = new JList(controller.listReaders());
        JLabel lbSearchWord = new JLabel("Search: ");
        JTextField tfSearchWord = new JTextField(20);
        JButton btSearchByName = new JButton("Name search");
        JButton btSearchByAddress = new JButton("Title search");
        JButton btBack = new JButton("Back");

        ListReadersWindow.setSize(600, 600);
        ListReadersWindow.setLocationRelativeTo(null);
        ListReadersWindow.setTitle("List readers");
        ListReadersWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        ListReadersWindow.setResizable(false);
        ListReadersWindow.setLayout(new BorderLayout());

        lsReaders.setEnabled(false);
        JScrollPane spListReaders = new JScrollPane(lsReaders);

        ListReadersWindow.add(spListReaders, BorderLayout.CENTER);

        JPanel pnSearch = new JPanel(new GridLayout(2, 1));
        JPanel pnSearchField = new JPanel();
        pnSearchField.add(lbSearchWord);
        pnSearchField.add(tfSearchWord);
        pnSearchField.add(btSearchByName);
        pnSearchField.add(btSearchByAddress);
        pnSearch.add(pnSearchField);
        JPanel pnBack = new JPanel();
        pnBack.add(btBack);
        pnSearch.add(pnBack);

        ListReadersWindow.add(pnSearch, BorderLayout.SOUTH);

        ListReadersWindow.setVisible(true);

        btSearchByName.addActionListener(ae -> {
            ListReadersWindow.dispose();
            JFrame ListReadersSearchedByName = new JFrame();
            JList lsReadersSearchedByName = new JList(controller.searchReaders(tfSearchWord.getText(), "name"));
            JButton btBack2 = new JButton("Back");

            ListReadersSearchedByName.setSize(600, 600);
            ListReadersSearchedByName.setLocationRelativeTo(null);
            ListReadersSearchedByName.setTitle("Search result");
            ListReadersSearchedByName.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            ListReadersSearchedByName.setResizable(false);
            ListReadersSearchedByName.setLayout(new BorderLayout());

            lsReadersSearchedByName.setEnabled(false);
            JScrollPane spListReadersSeaechedByName = new JScrollPane(lsReadersSearchedByName);

            ListReadersSearchedByName.add(spListReadersSeaechedByName, BorderLayout.CENTER);

            JPanel pnBack2 = new JPanel();
            pnBack2.add(btBack2);
            ListReadersSearchedByName.add(pnBack2, BorderLayout.SOUTH);

            ListReadersSearchedByName.setVisible(true);

            btBack2.addActionListener(ae2 -> {
                showListReadersWindow();
                ListReadersSearchedByName.dispose();
            }
            );
        }
        );

        btSearchByAddress.addActionListener(ae -> {
            ListReadersWindow.dispose();
            JFrame ListReadersSearchedByAddress = new JFrame();
            JList lsReadersSearchedByAddress = new JList(controller.searchReaders(tfSearchWord.getText(), "address"));
            JButton btBack2 = new JButton("Back");

            ListReadersSearchedByAddress.setSize(600, 600);
            ListReadersSearchedByAddress.setLocationRelativeTo(null);
            ListReadersSearchedByAddress.setTitle("Search result");
            ListReadersSearchedByAddress.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            ListReadersSearchedByAddress.setResizable(false);
            ListReadersSearchedByAddress.setLayout(new BorderLayout());

            lsReadersSearchedByAddress.setEnabled(false);
            JScrollPane spListReadersSeaechedByName = new JScrollPane(lsReadersSearchedByAddress);

            ListReadersSearchedByAddress.add(spListReadersSeaechedByName, BorderLayout.CENTER);

            JPanel pnBack2 = new JPanel();
            pnBack2.add(btBack2);
            ListReadersSearchedByAddress.add(pnBack2, BorderLayout.SOUTH);

            ListReadersSearchedByAddress.setVisible(true);

            btBack2.addActionListener(ae2 -> {
                showListReadersWindow();
                ListReadersSearchedByAddress.dispose();
            }
            );
        }
        );

        btBack.addActionListener(ae -> {
            showReadersWindow();
            ListReadersWindow.dispose();
        }
        );
    }

//Új olvasó felvétele
    public void showAddNewReaderWindow() {
        AddNewReaderWindow = new JFrame();

        JLabel lbID = new JLabel("Reader ID:");
        JTextField tfID = new JTextField(controller.generateIDNumber(), 10);
        JLabel lbName = new JLabel("Name: ");
        JTextField tfName = new JTextField(20);
        JLabel lbAddress = new JLabel("Title: ");
        JTextField tfAddress = new JTextField(20);
        JButton btAdd = new JButton("Add");
        JButton btBack = new JButton("Back");

        AddNewReaderWindow.setSize(300, 200);
        AddNewReaderWindow.setLocationRelativeTo(null);
        AddNewReaderWindow.setTitle("add new reader");
        AddNewReaderWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AddNewReaderWindow.setResizable(false);
        AddNewReaderWindow.setLayout(new GridLayout(5, 1));

        JPanel pnID = new JPanel();
        pnID.add(lbID);
        pnID.add(tfID);

        JPanel pnName = new JPanel();
        pnName.add(lbName);
        pnName.add(tfName);

        JPanel pnAddress = new JPanel();
        pnAddress.add(lbAddress);
        pnAddress.add(tfAddress);

        AddNewReaderWindow.add(pnID);
        AddNewReaderWindow.add(pnName);
        AddNewReaderWindow.add(pnAddress);
        AddNewReaderWindow.add(btAdd);
        AddNewReaderWindow.add(btBack);

        AddNewReaderWindow.setVisible(true);

        btAdd.addActionListener(ae -> {
            String id = tfID.getText();
            String name = tfName.getText();
            String address = tfAddress.getText();
            controller.addNewReader(id, name, address);
            AddNewReaderWindow.dispose();
            showReadersWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showReadersWindow();
            AddNewReaderWindow.dispose();
        }
        );

    }

//KÖLCSÖNZÉS    
    public void showBorrowWindow() {
        BorrowWindow = new JFrame();

        JButton btBorrowBooks = new JButton("Borrow books");
        JButton btBoooksBack = new JButton("Take back books");
        JButton btBack = new JButton("Back");

        BorrowWindow.setSize(300, 200);
        BorrowWindow.setLocationRelativeTo(null);
        BorrowWindow.setTitle("Borrow");
        BorrowWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        BorrowWindow.setResizable(false);

        JPanel pnBorrowMenu = new JPanel(new GridLayout(3, 1));
        pnBorrowMenu.add(btBorrowBooks);
        pnBorrowMenu.add(btBoooksBack);
        pnBorrowMenu.add(btBack);

        BorrowWindow.add(pnBorrowMenu);

        BorrowWindow.setVisible(true);

        btBorrowBooks.addActionListener(ae -> {
            showBorrowBooksWindow();
            BorrowWindow.dispose();
        }
        );

        btBoooksBack.addActionListener(ae -> {
            showBooksBackWindow();
            BorrowWindow.dispose();
        }
        );

        btBack.addActionListener(ae -> {
            showMenu();
            BorrowWindow.dispose();
        }
        );
    }

//Kölcsönzés - Könyvek kikölcsönzése
    public void showBorrowBooksWindow() {
        BorrowBooksWindow = new JFrame();

        JLabel lbReaders = new JLabel("Readers");
        JList lsReaders = new JList(controller.listReaders());
        lsReaders.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JLabel lbAvailableBooks = new JLabel("Available books");
        JList lsAvailableBooks = new JList(controller.listBooks("available"));
        lsAvailableBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JLabel lbBorrowID = new JLabel("Borrow ID");
        JTextField tfBorrowID = new JTextField(controller.generateIDNumber(), 10);
        JButton btBorrow = new JButton("Borrow book");
        JButton btBack = new JButton("Back");

        BorrowBooksWindow.setSize(1000, 800);
        BorrowBooksWindow.setLocationRelativeTo(null);
        BorrowBooksWindow.setTitle("Borrow books");
        BorrowBooksWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        BorrowBooksWindow.setResizable(false);
        BorrowBooksWindow.setLayout(new BorderLayout());

        JScrollPane spListReaders = new JScrollPane(lsReaders);
        JScrollPane spAvailableBooks = new JScrollPane(lsAvailableBooks);

        JPanel pnReaders = new JPanel(new BorderLayout());
        pnReaders.add(lbReaders, BorderLayout.NORTH);
        pnReaders.add(spListReaders, BorderLayout.CENTER);

        JPanel pnAvailableBooks = new JPanel(new BorderLayout());
        pnAvailableBooks.add(lbAvailableBooks, BorderLayout.NORTH);
        pnAvailableBooks.add(spAvailableBooks, BorderLayout.CENTER);

        JPanel pnLists = new JPanel(new GridLayout(1, 2));
        pnLists.add(pnReaders);
        pnLists.add(pnAvailableBooks);

        BorrowBooksWindow.add(pnLists, BorderLayout.CENTER);

        JPanel pnButtons = new JPanel(new GridLayout(3, 1));
        JPanel pnBorrowID = new JPanel();
        pnBorrowID.add(lbBorrowID);
        pnBorrowID.add(tfBorrowID);
        pnButtons.add(pnBorrowID);
        JPanel pnBorrow = new JPanel();
        pnBorrow.add(btBorrow);
        pnButtons.add(pnBorrow);
        JPanel pnBack = new JPanel();
        pnBack.add(btBack);
        pnButtons.add(pnBack);

        BorrowBooksWindow.add(pnButtons, BorderLayout.SOUTH);

        BorrowBooksWindow.setVisible(true);

        btBorrow.addActionListener(ae -> {
            String borrowID = tfBorrowID.getText();
            Object reader = lsReaders.getSelectedValue();
            Object book = lsAvailableBooks.getSelectedValue();

            controller.makeTheBorrow(borrowID, reader, book);

            BorrowBooksWindow.dispose();
            showBorrowWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showBorrowWindow();
            BorrowBooksWindow.dispose();
        }
        );

    }

//Kölcsönzés - Könyvek visszavétele
    public void showBooksBackWindow() {
        BooksBackWindow = new JFrame();

        JLabel lbActiveBorrows = new JLabel("Active borrows");
        JList lsActiveBorrows = new JList(controller.listBorrows("active"));
        lsActiveBorrows.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JLabel lbPerformedBorrows = new JLabel("Borrows done");
        JList lsPerformedBorrows = new JList(controller.listBorrows("performed"));
        lsPerformedBorrows.setEnabled(false);

        JButton btBooksBack = new JButton("Borrow confirmed");
        JButton btBack = new JButton("Back");

        BooksBackWindow.setSize(1300, 600);
        BooksBackWindow.setLocationRelativeTo(null);
        BooksBackWindow.setTitle("Book back");
        BooksBackWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        BooksBackWindow.setResizable(false);
        BooksBackWindow.setLayout(new BorderLayout());

        JScrollPane spListReaders = new JScrollPane(lsActiveBorrows);
        JScrollPane spAvailableBooks = new JScrollPane(lsPerformedBorrows);

        JPanel pnReaders = new JPanel(new BorderLayout());
        pnReaders.add(lbActiveBorrows, BorderLayout.NORTH);
        pnReaders.add(spListReaders, BorderLayout.CENTER);

        JPanel pnAvailableBooks = new JPanel(new BorderLayout());
        pnAvailableBooks.add(lbPerformedBorrows, BorderLayout.NORTH);
        pnAvailableBooks.add(spAvailableBooks, BorderLayout.CENTER);

        JPanel pnLists = new JPanel(new GridLayout(1, 2));
        pnLists.add(pnReaders);
        pnLists.add(pnAvailableBooks);

        BooksBackWindow.add(pnLists, BorderLayout.CENTER);

        JPanel pnButtons = new JPanel(new GridLayout(2, 1));

        JPanel pnBooksBack = new JPanel();
        pnBooksBack.add(btBooksBack);
        pnButtons.add(pnBooksBack);

        JPanel pnBack = new JPanel();
        pnBack.add(btBack);
        pnButtons.add(pnBack);

        BooksBackWindow.add(pnButtons, BorderLayout.SOUTH);

        BooksBackWindow.setVisible(true);

        btBooksBack.addActionListener(ae -> {
            Object activeBorrow = lsActiveBorrows.getSelectedValue();
            controller.makeTheBorrowPerformed(activeBorrow);

            BooksBackWindow.dispose();
            showBorrowWindow();
        }
        );

        btBack.addActionListener(ae -> {
            showBorrowWindow();
            BooksBackWindow.dispose();
        }
        );

    }
/*
//E_KÖNYVTÁR    
    public void showELibraryWindow() {
        ELibraryWindow = new JFrame();

        JLabel lbBooksInELibrary = new JLabel("e-könytárban meglevő könyvek");
        JList lsBooksInELibrary = new JList(controller.listBooksIsInELibrary("yes"));
        lsBooksInELibrary.setEnabled(false);

        JLabel lbBooks = new JLabel("Könyvtárban levő könyvek");
        JList lsBooks = new JList(controller.listBooksIsInELibrary("no"));
        lsBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JButton btAddToELibrary = new JButton("Felvétel az e-könyvtárba");
        JButton btBack = new JButton("Vissza");

        ELibraryWindow.setSize(1300, 600);
        ELibraryWindow.setLocationRelativeTo(null);
        ELibraryWindow.setTitle("e-könytár");
        ELibraryWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        ELibraryWindow.setResizable(false);
        ELibraryWindow.setLayout(new BorderLayout());

        JScrollPane spListBooksInELibrary = new JScrollPane(lsBooksInELibrary);
        JScrollPane spBooks = new JScrollPane(lsBooks);

        JPanel pnBooksInELibrary = new JPanel(new BorderLayout());
        pnBooksInELibrary.add(lbBooksInELibrary, BorderLayout.NORTH);
        pnBooksInELibrary.add(spListBooksInELibrary, BorderLayout.CENTER);

        JPanel pnBooks = new JPanel(new BorderLayout());
        pnBooks.add(lbBooks, BorderLayout.NORTH);
        pnBooks.add(spBooks, BorderLayout.CENTER);

        JPanel pnLists = new JPanel(new GridLayout(1, 2));
        pnLists.add(pnBooksInELibrary);
        pnLists.add(pnBooks);

        ELibraryWindow.add(pnLists, BorderLayout.CENTER);

        JPanel pnButtons = new JPanel(new GridLayout(2, 1));

        JPanel pnBooksBack = new JPanel();
        pnBooksBack.add(btAddToELibrary);
        pnButtons.add(pnBooksBack);

        JPanel pnBack = new JPanel();
        pnBack.add(btBack);
        pnButtons.add(pnBack);

        ELibraryWindow.add(pnButtons, BorderLayout.SOUTH);

        ELibraryWindow.setVisible(true);

        btAddToELibrary.addActionListener(ae -> {
            Object book = lsBooks.getSelectedValue();
            controller.putTheBookToTheELibrary(book);

            showMenu();
            ELibraryWindow.dispose();
        }
        );

        btBack.addActionListener(ae -> {
            showMenu();
            ELibraryWindow.dispose();
        }
        );
    }
*/
//ÜZENET A FELHASZNÁLÓNAK
    public void approveMessage(JFrame jframe, String message) {
        JOptionPane.showMessageDialog(jframe, message, "Success", JOptionPane.INFORMATION_MESSAGE);
    }

    public void warningMessage(JFrame jframe, String message) {
        JOptionPane.showMessageDialog(jframe, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

//GETTEREK
    public JFrame getMenuWindow() {
        return MenuWindow;
    }

    public JFrame getListBooksWindow() {
        return ListBooksWindow;
    }

    public JFrame getAddNewBookWindow() {
        return AddNewBookWindow;
    }

    public JFrame getAddNewBookTypeWindow() {
        return AddNewBookTypeWindow;
    }

    public JFrame getAddNewAuthorWindow() {
        return AddNewAuthorWindow;
    }

    public JFrame getBooksMenuWindow() {
        return BooksMenuWindow;
    }

    public JFrame getAddNewReaderWindow() {
        return AddNewReaderWindow;
    }

    public JFrame getListReadersWindow() {
        return ListReadersWindow;
    }

    public JFrame getReadersMenuWindow() {
        return ReadersMenuWindow;
    }

    public JFrame getBorrowWindow() {
        return BorrowWindow;
    }

    public JFrame getBooksBackWindow() {
        return BooksBackWindow;
    }

    public JFrame getBorrowBooksWindow() {
        return BorrowBooksWindow;
    }

    public JFrame getELibraryWindow() {
        return ELibraryWindow;
    }

}


