/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryapp.controller;

/**
 *
 * @author Norbi
 */
    
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import libraryapp.view.View;

/**
 *
 * @author Csaba
 */
public class Controller {

    private String url = "jdbc:oracle:thin:@localhost:1521:xe";
    private String username = "Norbi";
    private String password = "Norbi";

    private Connection con = null;

    private View view;

    private final int MINID = 1;
    private final int MAXID = 99999;

    public Controller(View view) {
        this.view = view;
        view.setController(this);
        view.showMenu();
    }

//KÖNYVEK
    //Könyvek listázása - kölcsönözhető és kikölcsönzött könyvek listázása
    public String[] listBooks(String borrowStatus) {
        List<String> listBooks = new ArrayList<>();
        String[] ArrayOfBooks = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = null;
            if (borrowStatus.equals("available")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = true GROUP BY kp.konyv_azonosito;";
            } else if (borrowStatus.equals("borrowed")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = false GROUP BY kp.konyv_azonosito;";
            }

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listBooks.add(rs.getString(1) + "; " + rs.getString(2) + "; " + rs.getString(3) + "; " + rs.getString(4) + "; " + rs.getString(5));
            }

            ArrayOfBooks = new String[listBooks.size()];
            for (int i = 0; i < listBooks.size(); i++) {
                ArrayOfBooks[i] = listBooks.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getListBooksWindow(), "Könyvek listázása sikertelen");
        }
        return ArrayOfBooks;
    }

    //Könyvek listázása keresés során
    public String[] searchBooks(String text, String nameOrTitle, String borrowStatus) {
        List<String> listBooks = new ArrayList<>();
        String[] ArrayOfBooks = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = null;
            if (nameOrTitle.equals("author") && borrowStatus.equals("available")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = true AND sz.szerzo_nev LIKE '%" + text + "%'GROUP BY kp.konyv_azonosito;";
            } else if (nameOrTitle.equals("author") && borrowStatus.equals("borrowed")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = false AND sz.szerzo_nev LIKE '%" + text + "%'GROUP BY kp.konyv_azonosito;";
            } else if (nameOrTitle.equals("title") && borrowStatus.equals("available")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = true AND k.cim LIKE '%" + text + "%'GROUP BY kp.konyv_azonosito;";
            } else if (nameOrTitle.equals("title") && borrowStatus.equals("borrowed")) {
                sql = "SELECT kp.konyv_azonosito, kp.isbn, k.cim, ksz.szerzo_id, sz.szerzo_nev FROM konyv_peldany kp LEFT JOIN konyv k ON kp.isbn = k.isbn LEFT JOIN konyvszerzo ksz ON k.isbn = ksz.isbn LEFT JOIN szerzo sz ON ksz.szerzo_id = sz.szerzo_id WHERE kp.kolcsonozheto = false AND k.cim LIKE '%" + text + "%'GROUP BY kp.konyv_azonosito;";
            }

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listBooks.add(rs.getString(1) + "; " + rs.getString(2) + "; " + rs.getString(3) + "; " + rs.getString(4) + "; " + rs.getString(5));
            }

            ArrayOfBooks = new String[listBooks.size()];
            for (int i = 0; i < listBooks.size(); i++) {
                ArrayOfBooks[i] = listBooks.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getListBooksWindow(), "Könyvek listázása sikertelen");
        }
        return ArrayOfBooks;
    }

    //Új könyvpéldány felvétele - ISBN lekérdezése
    public String[] listISBN() {
        List<String> listISBN = new ArrayList<>();
        String[] ArrayOfISBN = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "SELECT * FROM konyv";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listISBN.add(rs.getString("isbn"));
            }

            ArrayOfISBN = new String[listISBN.size()];
            for (int i = 0; i < listISBN.size(); i++) {
                ArrayOfISBN[i] = listISBN.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewBookWindow(), "ISBN listázása sikertelen");
        }
        return ArrayOfISBN;
    }

    //Új könyvpéldány felvétele - Könyvpéldány feltöltése a "konyv_peldany" táblába
    public void addBookToKonyvPeldanyTable(String BookID, Object ISBN) {

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT INTO konyv_peldany(konyv_azonosito, isbn, kolcsonozheto) VALUES ('KA-" + BookID + "','" + (String) ISBN + "', true)";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getAddNewBookWindow(), "Új könyvpéldány sikeresen felvéve.");
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewBookWindow(), "Új könyvpéldány felvétele sikertelen.");
        }
    }

    //Új könyv felvétele - Szerzők id-jának lekérdezése
    public String[] listAuthorID() {
        List<String> listAuthorID = new ArrayList<>();
        String[] ArrayOfAuthorID = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "SELECT * FROM szerzo";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listAuthorID.add(rs.getString("szerzo_id"));
            }

            ArrayOfAuthorID = new String[listAuthorID.size()];
            for (int i = 0; i < listAuthorID.size(); i++) {
                ArrayOfAuthorID[i] = listAuthorID.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewBookTypeWindow(), "Szerzők listázása sikertelen");
        }
        return ArrayOfAuthorID;
    }

    //Új könyv felvétele - Adatok feltöltése a "könyv" táblába
    public void addBookToKonyvTable(String ISBN, String title) {

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT INTO konyv(isbn, cim, megvan_az_Ekonyvtarban) VALUES ('ISBN-" + ISBN + "','" + title + "', false);";
            Statement st = con.createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewBookTypeWindow(), "Új könyv felvétele az adatbázis konyv táblájába sikertelen.");
        }
    }

    //Új könyv felvétele - Adatok feltöltése a "könyvszerzők" táblába
    public void addBookToKonyszerzoTable(String ISBN, Object[] authors) {
        try {
            con = DriverManager.getConnection(url, username, password);
            for (int i = 0; i < authors.length; i++) {
                String sql = "INSERT INTO konyvszerzo(isbn, szerzo_id) VALUES ('ISBN-" + ISBN + "','" + (String) (authors[i]) + "');";
                Statement st = con.createStatement();
                st.execute(sql);
            }
            view.approveMessage(view.getAddNewBookTypeWindow(), "Új könyv sikeresen felvéve.");
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewBookTypeWindow(), "Új könyv felvétele az adatbázis konyvszerzo táblájába sikertelen.");
        }
    }

    //Új szerző felvétele
    public void addNewAuthor(String id, String name) {

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT INTO szerzo(szerzo_id, szerzo_nev) VALUES ('SZ-" + id + "','" + name + "');";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getAddNewAuthorWindow(), "Új szerző sikeresen felvéve.");
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewAuthorWindow(), "Új szerző felvétele sikertelen.");
        }
    }

//OLVASÓK
    //Olvasók listázása
    public String[] listReaders() {
        List<String> listOfReaders = new ArrayList<>();
        String[] ArrayOfReaders = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "SELECT * FROM olvaso";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listOfReaders.add(rs.getString("olvaso_azonosito") + "; " + rs.getString("nev") + "; " + rs.getString("cim"));
            }

            ArrayOfReaders = new String[listOfReaders.size()];
            for (int i = 0; i < listOfReaders.size(); i++) {
                ArrayOfReaders[i] = listOfReaders.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getListReadersWindow(), "Olvasók listázása sikertelen");
        }
        return ArrayOfReaders;
    }

    //Keresés az olvasók között
    public String[] searchReaders(String text, String choice) {
        List<String> listOfReaders = new ArrayList<>();
        String[] ArrayOfReaders = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = null;
            if (choice.equals("name")) {
                sql = "SELECT * FROM olvaso WHERE nev LIKE '%" + text + "%';";
            } else if (choice.equals("address")) {
                sql = "SELECT * FROM olvaso WHERE cim LIKE '%" + text + "%';";
            }
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listOfReaders.add(rs.getString("olvaso_azonosito") + ", " + rs.getString("nev") + ", " + rs.getString("cim"));
            }

            ArrayOfReaders = new String[listOfReaders.size()];
            for (int i = 0; i < listOfReaders.size(); i++) {
                ArrayOfReaders[i] = listOfReaders.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getListReadersWindow(), "Olvasó keresése sikertelen.");
        }
        return ArrayOfReaders;
    }

    //Új olvasó felvétele
    public void addNewReader(String id, String name, String address) {

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT INTO olvaso(olvaso_azonosito, nev, cim) VALUES ('OA-" + id + "','" + name + "','" + address + "');";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getAddNewReaderWindow(), "Új olvasó sikeresen felvéve.");
        } catch (SQLException ex) {
            view.warningMessage(view.getAddNewReaderWindow(), "Új olvasó felvétele sikertelen.");
        }
    }

//KÖLCSÖNZÉS
    //Könyv kölcsönzése
    public void makeTheBorrow(String borrowID, Object reader, Object book) {

        String readerID = ((String) reader).substring(0, ((String) reader).indexOf(";"));

        String bookID = ((String) book).substring(0, ((String) book).indexOf(";"));

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "UPDATE konyv_peldany SET kolcsonozheto = false WHERE konyv_azonosito = '" + bookID + "';";
            Statement st = con.createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            view.warningMessage(view.getBorrowWindow(), "konyv_peldany tábla frissítése sikertelen");
        }

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT INTO kolcsonoz(kolcs_azonosito, olvaso_azonosito, konyv_azonosito, kiv_date, vissz_date) VALUES ('KCSA-" + borrowID + "','" + readerID + "','" + bookID + "', NOW(), null);";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getBorrowWindow(), "Kölcsönzés sikeres");
        } catch (SQLException ex) {
            view.warningMessage(view.getBorrowWindow(), "Kölcsönzés sikertelen");
        }
    }

    //Könyv visszavétele - Aktív és teljesített kölcsönzések listázása
    public String[] listBorrows(String borrowStatus) {
        List<String> listOfActiveBorrows = new ArrayList<>();
        String[] ArrayOfActiveBorrows = null;

        if (borrowStatus.equals("active")) {
            try {
                con = DriverManager.getConnection(url, username, password);

                String sql = "SELECT kcs.kolcs_azonosito, kcs.olvaso_azonosito, o.nev, o.cim, kcs.konyv_azonosito, k.cim, kcs.kiv_date FROM kolcsonoz kcs LEFT JOIN olvaso o ON o.olvaso_azonosito = kcs.olvaso_azonosito LEFT JOIN konyv_peldany kp ON kp.konyv_azonosito = kcs.konyv_azonosito LEFT JOIN konyv k ON k.isbn = kp.isbn WHERE kcs.vissz_date IS null;";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    listOfActiveBorrows.add(rs.getString(1) + "; " + rs.getString(2) + "; " + rs.getString(3) + "; " + rs.getString(4) + "; " + rs.getString(5) + "; " + rs.getString(6) + "; " + rs.getString(7));
                }

                ArrayOfActiveBorrows = new String[listOfActiveBorrows.size()];
                for (int i = 0; i < listOfActiveBorrows.size(); i++) {
                    ArrayOfActiveBorrows[i] = listOfActiveBorrows.get(i);
                }
            } catch (SQLException ex) {
                view.warningMessage(view.getBooksBackWindow(), "Aktív kölcsönzések listázása sikertelen");
            }
        } else if (borrowStatus.equals("performed")) {
            try {
                con = DriverManager.getConnection(url, username, password);

                String sql = "SELECT kcs.kolcs_azonosito, kcs.olvaso_azonosito, o.nev, o.cim, kcs.konyv_azonosito, k.cim, kcs.kiv_date, kcs.vissz_date FROM kolcsonoz kcs LEFT JOIN olvaso o ON o.olvaso_azonosito = kcs.olvaso_azonosito LEFT JOIN konyv_peldany kp ON kp.konyv_azonosito = kcs.konyv_azonosito LEFT JOIN konyv k ON k.isbn = kp.isbn WHERE kcs.vissz_date IS NOT null;";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    listOfActiveBorrows.add(rs.getString(1) + "; " + rs.getString(2) + "; " + rs.getString(3) + "; " + rs.getString(4) + "; " + rs.getString(5) + "; " + rs.getString(6) + "; " + rs.getString(7) + "; " + rs.getString(8));
                }

                ArrayOfActiveBorrows = new String[listOfActiveBorrows.size()];
                for (int i = 0; i < listOfActiveBorrows.size(); i++) {
                    ArrayOfActiveBorrows[i] = listOfActiveBorrows.get(i);
                }
            } catch (SQLException ex) {
                view.warningMessage(view.getBooksBackWindow(), "Teljesített kölcsönzések listázása sikertelen");
            }
        }

        return ArrayOfActiveBorrows;
    }
    
    //Könyv visszavétele - kölcsönzés teljesítése
    public void makeTheBorrowPerformed(Object activeBorrow) {
        String activeBorrowString = (String) activeBorrow;
        
        String borrowID = activeBorrowString.substring(0, activeBorrowString.indexOf(";"));
        
        String stringWithoutKCSA = activeBorrowString.substring(activeBorrowString.indexOf(";") + 2);
        String stringWithoutOA = stringWithoutKCSA.substring(stringWithoutKCSA.indexOf(";") + 2);
        String stringWithoutName = stringWithoutOA.substring(stringWithoutOA.indexOf(";") + 2);
        String stringWithoutAddress = stringWithoutName.substring(stringWithoutName.indexOf(";") + 2);
        
        String bookID = stringWithoutAddress.substring(0, stringWithoutAddress.indexOf(";"));
        
        //konyv_peldany tábla frissitese
        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "UPDATE konyv_peldany SET kolcsonozheto = true WHERE konyv_azonosito = '" + bookID + "';";
            Statement st = con.createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            view.warningMessage(view.getBooksBackWindow(), "konyv_peldany tábla frissítése sikertelen");
        }
        
        //kolcsonoz tábla frissitese
        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "UPDATE kolcsonoz SET vissz_date = NOW() WHERE kolcs_azonosito = '" + borrowID + "';";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getBooksBackWindow(), "Kölcsönzés teljesítve");
        } catch (SQLException ex) {
            view.warningMessage(view.getBooksBackWindow(), "kolcsonoz tábla frissítése sikertelen");
        }
    }
    
//E-könytár
    //Könyvek listázása - megvan-e az e-könyvtárban
    public String[] listBooksIsInELibrary(String bookInELibrary) {
        List<String> listOfBooks = new ArrayList<>();
        String[] ArrayOfBooks = null;

        try {
            con = DriverManager.getConnection(url, username, password);
            
            String sql = null;
            if (bookInELibrary.equals("yes")) {
                sql = "SELECT k.isbn, k.cim FROM konyv k WHERE k.megvan_az_Ekonyvtarban = true;";
            } else if (bookInELibrary.equals("no")) {
                sql = "SELECT k.isbn, k.cim FROM konyv k WHERE k.megvan_az_Ekonyvtarban = false;";
            }
            
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listOfBooks.add(rs.getString(1) + "; " + rs.getString(2));
            }

            ArrayOfBooks = new String[listOfBooks.size()];
            for (int i = 0; i < listOfBooks.size(); i++) {
                ArrayOfBooks[i] = listOfBooks.get(i);
            }
        } catch (SQLException ex) {
            view.warningMessage(view.getELibraryWindow(), "Könyvek listázása sikertelen");
        }
        return ArrayOfBooks;
    }
    
    //Könyv felvétele az e-könyvtárba
    public void putTheBookToTheELibrary(Object book) {
        String bookString = (String) book;
        String ISBN = bookString.substring(0, bookString.indexOf(";"));
        
        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "UPDATE konyv SET megvan_az_Ekonyvtarban = true WHERE isbn = '" + ISBN + "';";
            Statement st = con.createStatement();
            st.execute(sql);
            view.approveMessage(view.getELibraryWindow(), "A könyv sikeresen felvéve az e-könyvtárba.");
        } catch (SQLException ex) {
            view.warningMessage(view.getELibraryWindow(), "A könyv felvétele az e-könyvtárban sikertelen.");
        }
    }

//ID GENERÁLÁS
    public String generateIDNumber() {
        int iD = (int) ((MAXID - MINID + 1) * Math.random() + MINID);
        String IDNumber = Integer.toString(iD);
        return IDNumber;
    }

//KILÉPÉS A PROGRAMBÓL    
    public void closeTheProgram() {
        System.exit(0);
    }

}
