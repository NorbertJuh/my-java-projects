
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.loginexample.servlets;

import com.bh08.loginexample.beans.LoginBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Norbi
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");
        if (null == loginBean) {
            loginBean = new LoginBean();
            session.setAttribute("loginBean", loginBean);
        }

        super.service(request, response); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");

        if (loginBean.isValidLogin() && "true".equals(request.getParameter("reset"))) {
            loginBean.reset();
        }

        request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        loginBean.clearFlash(); //ez a Flash-scope
        System.out.println("GET");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");

        System.out.println("POST");

        String loginName = request.getParameter("username");
        String password = request.getParameter("password");
        //HW: EJB (@Stateles, @LocalBean), vagy DAO-s login elkészítése

        loginBean.setNumOfTry(loginBean.getNumOfTry() + 1);

        if ("user".equals(loginName) && "password".equals(password)) {
            loginBean.setValidLogin(true);
            synchronized (this) {
                Integer maxTry = (Integer) getServletContext().getAttribute("maxTry");
                if (null == maxTry || maxTry < loginBean.getNumOfTry()) {
                    maxTry = loginBean.getNumOfTry();
                    getServletContext().setAttribute("maxTry", maxTry);
                }
            }
        } else {
            loginBean.setInvalidLogin(true);
        }
        //request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
