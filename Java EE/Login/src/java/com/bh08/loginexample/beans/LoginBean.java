/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.loginexample.beans;

import java.io.Serializable;

/**
 *
 * @author Norbi
 */
public class LoginBean implements Serializable {
    
    private boolean validLogin;
    private boolean invalidLogin;
    private int numOfTry;

    public LoginBean() {
        reset();
    }
    
    public void reset(){
        this.validLogin = false;
        this.invalidLogin = false;
        this.numOfTry = 0;
    }
    
    public void clearFlash() {
        setInvalidLogin(false);
    }

    public boolean isValidLogin() {
        return validLogin;
    }

    public void setValidLogin(boolean validLogin) {
        this.validLogin = validLogin;
    }

    public boolean isInvalidLogin() {
        return invalidLogin;
    }

    public void setInvalidLogin(boolean invalidLogin) {
        this.invalidLogin = invalidLogin;
    }

    public int getNumOfTry() {
        return numOfTry;
    }

    public void setNumOfTry(int numOfTry) {
        this.numOfTry = numOfTry;
    }
    
}
