<%-- 
    Document   : login
    Created on : 2019.02.16., 12:03:40
    Author     : Norbi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <c:if test="${loginBean.validLogin}">
            <label>Sikerült bejelentkeznem!</label><br />
            <label>Próbálkozások száma</label>
            <c:out value="${loginBean.numOfTry}"/><br />
            <label>Max próbálkozások száma</label>
            <c:out value="${maxTry}"/><br />
            <a href="?reset=true" >Reset...</a>
        </c:if>

        <c:if test="${!loginBean.validLogin}">
            <h3>Login form</h3><br/>
            <form method="POST" enctype="application/x-www-form-urlencoded">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>
                <br>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>
                <br>
                <button type="submit">Login</button>
            </form><br />
            <label>Próbálkozások száma</label>
            <c:out value="${loginBean.numOfTry}"/><br />
        </c:if>
        <c:if test="${loginBean.invalidLogin}">
            <label>Sikertelen bejelentkezés!</label><br />
        </c:if>
    </body>
</html>
