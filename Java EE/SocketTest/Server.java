/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Norbi
 */
public class Server {

    
    public static void main(String[] args) {
        
        try (ServerSocket serverSocket = new ServerSocket(999)) {
            System.out.println("Server is running");
            while(true){
            Socket socket = serverSocket.accept();
                System.out.println("Client connected");
            new ClientThread(socket).start();
            } 
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    public static class ClientThread extends Thread {
    
        private Socket socket;

        public ClientThread(Socket socket) {
            this.socket = socket;
        }
        
        @Override
        public void run() {
            try(PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                    InputStream inputStream = socket.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {                                                
                printWriter.println("Hello World");
                String line;
                while((line=bufferedReader.readLine())!= null){
                    System.out.println(line);
                }
            } catch (IOException ex) {
               ex.printStackTrace();
            }
        }
    }
    
    
    
}
