/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettest;

import java.util.ArrayList;

/**
 *
 * @author Norbi
 */
public class Book {
    
    private String name;
    private int Isbn;
    private boolean isborrowed;
    private boolean isInRepo;
    public static ArrayList books = new ArrayList<>();
    public static ArrayList borrowedBooks = new ArrayList<>();
    public static ArrayList repoBooks = new ArrayList<>();

    public Book(String name, int Isbn, boolean isborrowed, boolean isInRepo) {
        this.name = name;
        this.Isbn = Isbn;
        this.isborrowed = isborrowed;
        this.isInRepo = isInRepo;
        books.add(this);
        if ((isborrowed))
            borrowedBooks.add(this);
        if ((isInRepo))
            repoBooks.add(this);
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsbn() {
        return Isbn;
    }

    public void setIsbn(int Isbn) {
        this.Isbn = Isbn;
    }

    public boolean isIsborrowed() {
        return isborrowed;
    }

    public void setIsborrowed(boolean isborrowed) {
        this.isborrowed = isborrowed;
    }

    public boolean isIsInRepo() {
        return isInRepo;
    }

    public void setIsInRepo(boolean isInRepo) {
        this.isInRepo = isInRepo;
    }

    @Override
    public String toString() {
        return "Book{" + "name=" + name + ", Isbn=" + Isbn + ", isborrowed=" + isborrowed + ", isInRepo=" + isInRepo + '}';
    }
    
    
    
}
