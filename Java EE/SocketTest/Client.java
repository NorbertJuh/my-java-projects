/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static sockettest.Book.books;

/**
 *
 * @author Norbi
 */
public class Client {

    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("localhost", 999);
                InputStream inputStream = socket.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));) {

            // Scanner scanner = new Scanner(System.in);
            Book book1 = new Book("Egri Csillagok", 3232, true, true);
            Book book2 = new Book("Star wars", 4544, false, true);
            Book book3 = new Book("Harry Potter", 9932, true, false);
            Book book4 = new Book("Java kezdőknek", 5532, true, true);
            Book book5 = new Book("Amerika Kapitány", 1112, false, false);

            
            
            List<Book> repo = new ArrayList<Book>();

    /*        for (Book book : books) {
                if (book.isIsborrowed() == true) {
                    repo.add(boo);
*/
                    try (PrintWriter printwriter = new PrintWriter(socket.getOutputStream(), true)) {
                        //while (true) {
                        // String userInput = scanner.nextLine();
                        System.out.println("List of books:");
                        printwriter.println(books);         //Socket printwriterre írunk
                        System.out.println("List of borrowed books:");
                        printwriter.println(Book.borrowedBooks);
                        System.out.println("List of books in the repo:");
                        printwriter.println(Book.repoBooks);
                       // printwriter.println(repo);
                        //}
                    }
                    /*         
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                
            }
                     */

                }
            }
        }
    

