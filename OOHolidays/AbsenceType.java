/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public enum AbsenceType {
    GENERAL, MATERNITY, SICKLEAVE, OTHER, UNPAIDLEAVE, STUDY, HOMEOFFICE, SECONDMENT, BUSINESSTRIP
}
