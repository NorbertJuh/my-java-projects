/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class BluecollarWorker extends Worker{
    
    private Manager manager;

    public BluecollarWorker(Manager manager, String name, int age) {
        super(name, age);
        this.manager = manager;
        this.holidayDays+=2;
    }

   
    public void addHolidayRequest(int length) {
        Request request;
        request = new Request(length,AbsenceType.HOMEOFFICE);
        this.manager.addRequest(request);
    
    } 

    
}
