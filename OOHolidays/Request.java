/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class Request {
    
    private int length;
    private AbsenceState state;
    private AbsenceType type;

    public Request(int length, AbsenceType type) {
        this.length = length;
        this.state = state;
        this.type = type;
        
        
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public AbsenceState getState() {
        return state;
    }

    public void setState(AbsenceState state) {
        this.state = state;
    }

    public AbsenceType getType() {
        return type;
    }

    public void setType(AbsenceType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Request{" + "length=" + length + ", state=" + state + ", type=" + type + '}';
    }
    
    
    
    
}
