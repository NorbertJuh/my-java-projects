/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class WhitecollarWorker extends Worker{
    
    private Manager manager;

    public WhitecollarWorker(Manager manager, String name, int age) {
        super(name, age);
        this.manager = manager;
    }

    
    public void addHolidayRequest(int length) {
        Request request;
        request = new Request(length,AbsenceType.GENERAL);
        this.manager.addRequest(request);
    
    } 

  
    
}
   