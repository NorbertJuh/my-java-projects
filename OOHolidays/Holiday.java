/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class Holiday {

   
    public static void main(String[] args) {
        UltimateManager Vili = new UltimateManager("Vili", 33);
        Manager Otto = new Manager(Vili,"Ottó",28);
        Worker Sanyi = new BluecollarWorker(Otto,"Sándor",42);
        
        Worker Robert = new WhitecollarWorker(Otto,"Róbert",33);
        Worker Daniel = new Manager(Vili,"Dániel",30);
        Worker Jozsef = new BluecollarWorker(Otto,"József",55);
        
        System.out.println(Jozsef);
        System.out.println(Worker.availableDaysByAge(Sanyi.getAge()));
        System.out.println(Worker.availableDaysByAge(Otto.getAge()));
        System.out.println(Jozsef.holidayDays);
        
        Jozsef.addHolidayRequest(6);
        Jozsef.addHolidayRequest(30);
        Jozsef.printRequests();
    }

    
    
}
