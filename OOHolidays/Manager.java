/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class Manager extends Worker{

    private UltimateManager ultimatemanager;

    public Manager(UltimateManager ultimatemanager, String name, int age) {
        super(name, age);
        this.ultimatemanager = ultimatemanager;
    }
    
    
    
    public void addHolidayRequest(int length) {
        Request request;
        request = new Request(length,AbsenceType.BUSINESSTRIP);
        this.ultimatemanager.addRequest(request);
    
    } 
    
}
