/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

import java.util.ArrayList;

/**
 *
 * @author Norbi
 */
public abstract class Worker {

    private static int nextId = 1;
    protected final int id;
    protected String name;
    protected int age;
    protected int holidayDays;
    protected ArrayList<Request> requests;

    public Worker(String name, int age) {
        this.name = name;
        this.age = age;
        this.id = nextId++;
        this.holidayDays = availableDaysByAge(age);
        this.requests = new ArrayList<Request>();

    }

    public abstract void addHolidayRequest(int length);

    public void addRequest(Request request) {
        this.requests.add(request);
    }
    
    public void RequestDecision (Request request) {
        if (request.getLength() > Worker.availableDaysByAge(age)) {
            request.setState(AbsenceState.REJECTED);
        } else {
            char decision;
            do {
                decision = extra.Console.readChar("The manager decision: 1. Approve 'a' 2. Pending  'p'\n3. Reject 'r'");
            } while (decision != 'a' && decision != 'p' && decision != 'r');

            if (decision == 'a') {
                request.setState(AbsenceState.APPROVED);
            } else if (decision == 'p') {
                request.setState(AbsenceState.PENDING);
            } else {
                request.setState(AbsenceState.REJECTED);
            }
        }
    }

    public void printRequests() {
        for (Request request : this.requests) {
            if (request.getState().equals(AbsenceState.PENDING)) {
                System.out.println(request);
            }
        }
    }

    public static int availableDaysByAge(int age) {
        int day = 0;
        if (age < 25) {
            day = 20;
        } else if (age < 28) {
            day = 21;
        } else if (age < 31) {
            day = 22;
        } else if (age < 33) {
            day = 23;
        } else if (age < 35) {
            day = 24;
        } else if (age < 37) {
            day = 25;
        } else if (age < 39) {
            day = 26;
        } else if (age < 41) {
            day = 27;
        } else if (age < 43) {
            day = 28;
        } else if (age < 45) {
            day = 29;
        } else if (age > 46) {
            day = 30;
        }

        return day;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHolidayDays() {
        return holidayDays;
    }

    public void setHolidayDays(int holidayDays) {
        this.holidayDays = holidayDays;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Worker other = (Worker) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public ArrayList<Request> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<Request> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return "Worker{" + "id=" + id + ", name=" + name + ", age=" + age + '}';
    }

}
