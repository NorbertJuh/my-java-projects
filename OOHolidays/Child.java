/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.holidays;

/**
 *
 * @author Norbi
 */
public class Child extends Worker{
    
    private boolean disabled;
    private Worker parent;

    public Child(boolean disabled, String name, int age, Worker parent) {
        super(name, age);
        this.disabled = disabled;
        this.parent = parent;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHolidayDays() {
        return holidayDays;
    }

    public void setHolidayDays(int holidayDays) {
        this.holidayDays = holidayDays;
    }

    public Worker getParent() {
        return parent;
    }

    public void setParent(Worker parent) {
        this.parent = parent;
    }

    
    
    
    
    
    @Override
    public void addHolidayRequest(int length) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
