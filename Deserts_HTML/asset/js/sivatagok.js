var deserts = [
            { "name": "Rub' al Háli", "size": 800000 },
            { "name": "Kalahári", "size": 518000 },
            { "name": "Nagy-homoksivatag", "size": 420000 },
            { "name": "Takla-Makán", "size": 400000 },
            { "name": "Szír-sivatag", "size": 350000 },
            { "name": "Nagy-Viktória-sivatag", "size": 330000 },
            { "name": "Kara-kum", "size": 300000 },
            { "name": "Núbiai-sivatag", "size": 250000 },
            { "name": "Kizil-kum", "size": 240000 },
            { "name": "Gibson-sivatag", "size": 220000 },
            { "name": "Simpson-sivatag", "size": 200000 }
        ];

        function generate() {

            var html = '<table class="moredesert" align="center"><tr><th>Sivatag neve</th><th>Mérete (km<sup>2</sup>)</th></tr>';

            for (var i = 0; i < deserts.length; i++) {
                html += '<tr><td>' + deserts[i].name + '</td>';
                html += '    <td>' + deserts[i].size + '</td>';
                html += '</tr>'
            }

            html += '</table>' +
                    '<br />' +
                    '<div class="wrapper">' +
                    '<button onclick="area()" align="center">Területük összege</button>' +
                    '</div>';

            var where = document.getElementById("deserts");

            where.innerHTML = html;

        }

       // function numberWithWhiteSpaces(number) {
          //  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //}

        function area() {

            var area = 0;

            for (var i = 0; i < deserts.length; i++) {
                area += deserts[i].size;
            }

            //area = numberWithWhiteSpaces(area);

            alert("A sivatagok területe: " + area + " négyzetkilométer.");

        }

        function mouseOver(picture) {

            if (picture === "Szahara") {
                window.open("https://www.youtube.com/watch?v=gOtGbzUtjiw");
            } else if (picture === "Gobi") {
                window.open("https://www.youtube.com/watch?v=LhNpQyRnghY");
            } else if (picture === "Thar") {
                window.open("https://www.youtube.com/watch?v=8TbI8IzSpoI");
            }

        }

