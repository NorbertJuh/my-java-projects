function setBorder() {
    document.getElementById("firstPicture").style.border = "thick solid green";
    document.getElementById("thirdPicture").style.border = "thick solid purple";
}

function hideSecondPicture() {
    document.getElementById("secondPicture").style.visibility = 'hidden';
}

function visibleSecondPicture() {
    document.getElementById("secondPicture").style.visibility = 'visible';
}
