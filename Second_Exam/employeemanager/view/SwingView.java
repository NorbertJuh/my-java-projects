/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.view;

import employeemanager.controller.EmployeeController;
import java.awt.AWTEventMulticaster;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;


/**
 *
 * @author Norbi
 */
public class SwingView extends JFrame implements IView {
    
    EmployeeController con;
    private JButton addButton;
    private JButton getButton;
    private JButton removeButton;
    private JButton listButton;
    private JTextArea textArea;
    private JTextField textField;
    private JButton submitButton;
    

    public SwingView() {
        
        
        //init();
    }
    
    private void init() {
        setSize(1000, 500);
        setLocationRelativeTo(null); //középre kerül az ablak
        
        setTitle("Employee Manager");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setLayout(new GridLayout(6,1));
        
        addButton = new JButton("Add");
        addButton.addActionListener((ActionEvent ae) -> {
            con.doSelected(2);//To change body of generated methods, choose Tools | Templates.
        });
        add(addButton);
        
        
        listButton = new JButton("List");
        listButton.addActionListener((ActionEvent ae) -> {
            con.doSelected(1);//To change body of generated methods, choose Tools | Templates.
        });
        add(listButton);
        
        
        
        textField = new JTextField();
        textField.setVisible(false);
        add(textField);
        
        submitButton = new JButton("Submit");
        submitButton.setVisible(false);
        add(submitButton);
        
        textArea = new JTextArea();
        textArea.setVisible(false);
        textArea.setLineWrap(true);
        add(textArea);
        
        setVisible(true);
    }

    @Override
    public void setController(EmployeeController con) {
       this.con = con;
    }
    
    @Override
    public void showEmployee(String employeeString) {
        textArea.setVisible(true);
        textArea.setText(employeeString);
    }

    @Override
    public void showEmployeeList(String listString) {
        textArea.setVisible(true);
        textArea.setText(listString);
    }

    @Override
    public void showMessage(String text) {
        textArea.setVisible(true);
        textArea.setText(text);
    }

    @Override
    public void showMenu() {
        init();
    }

    

    

    @Override
    public void doMenuAddEmployee() {
        textField.setVisible(true);
        submitButton.setVisible(true);
        submitButton.addActionListener((ActionEvent ae) -> {
            con.add(textField.getText());
        });
    }

   

    
}
