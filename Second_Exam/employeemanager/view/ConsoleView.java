/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.view;

import employeemanager.controller.EmployeeController;
import java.util.Scanner;

/**
 *
 * @author Norbi
 */
public class ConsoleView implements IView{
    
    EmployeeController con;

    @Override
    public void setController(EmployeeController con) {
        this.con = con;
    }

    @Override
    public void showEmployee(String employeeString) {
        System.out.println(employeeString);
    }

    @Override
    public void showEmployeeList(String listString) {
        
        System.out.println(listString);
    }

    @Override
    public void showMessage(String text) {
        System.out.println(text);
    }

    @Override
    public void showMenu() {
       
        int menuNumber;
        
        do{
            menuNumber = extra.Console.readInt("Choose a menu number: (3 Quit)\n"
                    + "1. list all employees\n"
                    + "2. add employee\n"
                    );
            
        switch (menuNumber){
            case 1:      
            con.doSelected(1); break;
            case 2:
            con.doSelected(2); break;
            
            
        }
        
    }while(menuNumber!=3);
        }

    

    @Override
    public void doMenuAddEmployee() {
        String employeeName = extra.Console.readLine("Type the name of the new employee: ");
        con.add(employeeName);
    }

    
    
}
