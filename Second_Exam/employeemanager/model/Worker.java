/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.model;

import java.io.Serializable;

/**
 *
 * @author Norbi
 */  
    public abstract class Worker implements Serializable{
        
    protected int id = 0;
    protected String name;
    

    public Worker(int id, String name) {
        this.id = id++;
        this.name = name;
       
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + '}';
    }

}


