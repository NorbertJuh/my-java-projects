/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.model;

import java.io.Serializable;

/**
 *
 * @author Norbi
 */
public class Employee extends Worker implements Serializable{
    
   private String boss;

    public Employee(String boss, int id, String name) {
        super(id, name);
        this.boss = boss;
    }

    public String getBoss() {
        return boss;
    }

    public void setBoss(String boss) {
        this.boss = boss;
    }

    @Override
    public String toString() {
        return "Employee{" + super.toString() + "boss=" + boss + '}';
    }
   
   
    
    

    
    
    
    
}
 
   