/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.model;

import java.io.Serializable;

/**
 *
 * @author Norbi
 */
public class Boss extends Worker implements Serializable{
    
    private String Employees;

    public Boss(String Employees, int id, String name) {
        super(id, name);
        this.Employees = Employees;
    }

    public String getEmployees() {
        return Employees;
    }

    public void setEmployees(String Employees) {
        this.Employees = Employees;
    }

    @Override
    public String toString() {
        return "Boss{" + super.toString() + "Employees=" + Employees + '}';
    }
    
    
    
}
