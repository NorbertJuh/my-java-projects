/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.controller;

import employeemanager.model.Boss;
import employeemanager.model.Employee;
import employeemanager.model.Worker;
import employeemanager.view.ConsoleView;
import employeemanager.view.IView;
import employeemanager.view.SwingView;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Norbi
 */
public class EmployeeController implements ViewInteraction, Serializable{
    
    private IView view;
    public static List<Worker> workers;
    private int lastId = 0;

    public EmployeeController() throws FileNotFoundException, IOException {
        this.workers = new ArrayList<>();
        generateEmployees();
        generateBosses();
     //   String employeePath = "C:\\Workspace\\employee.ser";
     //   try(FileOutputStream fout = new FileOutputStream(employeePath);
         //       ObjectOutputStream ou = new ObjectOutputStream(fout)){
               //     ou.writeObject(workers);  
         
    }
    
  
    public void initView(int viewNumber){
        
        if (viewNumber == 0)
            view = new ConsoleView();
        else 
            view = new SwingView();
        
        view.setController(this);
        view.showMenu();
    }
    
    private void generateEmployees(){
        for(int i = 1; i < 4; i++){
            Employee em = new Employee("Jani", i, "Employee" + i);
            workers.add(em);
            lastId++;
        }
    }
    
    private void generateBosses(){
        for(int i = 4; i < 8; i++){
            Boss boss = new Boss("Aladar, Bela", i, "Boss" + i);
            workers.add(boss);
            lastId++;
        }
    }

    @Override
    public void list() {
        view.showEmployeeList(workers.toString());
    }

    @Override
    public void add(String name) {
        Employee em = new Employee("Jozsi", ++lastId, name);
        workers.add(em);
        view.showMessage("" + em.getName() + " successfully created!");
        
        String employeePath = "C:\\Workspace\\employee.ser";
        
         try
        {
            FileOutputStream fos = new FileOutputStream(employeePath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(workers);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        
        
       /* try(FileOutputStream fout = new FileOutputStream(employeePath);
                ObjectOutputStream ou = new ObjectOutputStream(fout)){
                    ou.writeObject(workers);  */
        
    
        }
    }
   

    
    @Override
    public void doSelected(int selected) {
        switch(selected) {
            case 1: list(); break;     
            case 2: view.doMenuAddEmployee(); break;
            
        }
    }

    
    
    
}

