/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_swing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import zh2_swing.controller.Controller;
import zh2_swing.view.IView;
import zh2_swing.view.View;

/**
 *
 * @author Norbi
 */
public class Application {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        IView view = View.getInstance();
        Controller controller = new Controller(view);

    }

}
