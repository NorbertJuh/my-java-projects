/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_swing.controller;

/**
 *
 * @author Norbi
 */
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import zh2_swing.view.IView;


public class Controller {

    private IView view;
    private int id = 1;
    public static List<String> events = new ArrayList<>();
    LocalDateTime ldt = LocalDateTime.now();

    public Controller(IView view) throws FileNotFoundException, UnsupportedEncodingException {
        this.view = view;

        view.setController(this);
        view.showMenu();
        events.add("Program starts" + LocalDateTime.now());

    }

    public void listEvents() throws IOException {

        view.listEvents(events.toString());

        events.add("Event ID" + id + "   " + LocalDateTime.now() + "controller/Controller\n");
        id++;

        view.showMenu();
        writefile();
    }

    public void writefile() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        FileWriter writer = new FileWriter("output.txt");

        for (String str : events) {
            writer.write(str + "\n");
        }
        writer.close();
    }
}
