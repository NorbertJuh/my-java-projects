/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_swing.view;

/**
 *
 * @author Norbi
 */
import java.awt.GridLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import zh2_swing.controller.Controller;

public class View implements IView {

    private Controller controller;

    private static View view = null;

    private View() {
    }

    public static View getInstance() {
        if (view == null) {
            view = new View();
        }
        return view;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void showMenu() {
        JFrame firstWindow = new JFrame();

        JTextArea area = new JTextArea("Logger program");
        JButton btAdd = new JButton("Press button");

        firstWindow.setSize(500, 500);
        firstWindow.setLocationRelativeTo(null);
        firstWindow.setTitle("Logger");
        firstWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        firstWindow.setVisible(true);

        JPanel pnMenu = new JPanel(new GridLayout(2, 1));

        pnMenu.add(area);
        pnMenu.add(btAdd);

        firstWindow.add(pnMenu);

        btAdd.addActionListener(ae -> {
            try {
                controller.listEvents();
            } catch (IOException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            }
            firstWindow.dispose();
        }
        );

    }

    @Override
    public void listEvents(String events) {
        JFrame listWindow = new JFrame();

        JButton btBack = new JButton("Vissza!");

        JTextArea taText = new JTextArea();

        listWindow.setSize(500, 500);
        listWindow.setLocationRelativeTo(null);
        listWindow.setTitle("Logger");
        listWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        listWindow.setVisible(true);
        listWindow.setAlwaysOnTop(true);

        JPanel pnPanel = new JPanel(new GridLayout(2, 1));

        taText.setText(events);
        taText.setLineWrap(true);

        pnPanel.add(taText);
        pnPanel.add(btBack);

        listWindow.add(pnPanel);

        btBack.addActionListener(ae -> {
            listWindow.dispose();
        }
        );
    }

}
