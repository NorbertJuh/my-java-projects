/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_swing.view;

import zh2_swing.controller.Controller;

/**
 *
 * @author Norbi
 */

public interface IView {
    
    void setController(Controller controller);
    void showMenu();   
    void listEvents(String events);
    
}

