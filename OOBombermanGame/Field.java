/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import bomberman.modell.Edge;
import bomberman.modell.EmptyField;
import bomberman.modell.FieldUnit;
import bomberman.modell.Monster;
import bomberman.modell.Movable;
import bomberman.modell.Player;
import bomberman.modell.Treasure;
import bomberman.modell.Wall;

/**
 *
 * @author Norbi
 */
public class Field {

    private int maxX;
    private int maxY;
    private FieldUnit[][] units;
    private Movable[] movables;
    private Player player;

    public Field(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.units = new FieldUnit[maxX][maxY];
        initializeField();

    }

    public void initializeField() {
        boolean goodPlace = false;
        for (int i = 0; i < maxX; i++) {
            for (int j = 0; j < maxY; j++) {
                double randomNumber = Math.random();
                if (i == 0 || j == 0 || i == maxX - 1 || j == maxY - 1) {
                    units[i][j] = new Edge(i, j);
                } else if (randomNumber < 0.1) {
                    units[i][j] = new Monster(i, j);
                } else if (randomNumber < 0.14) {
                    units[i][j] = new Treasure(i, j);
                } else if (randomNumber < 0.3) {
                    units[i][j] = new Wall(i, j);

                } else {
                    units[i][j] = new EmptyField(i, j);
                }
            }
        }

        
        do {

            int playerX = (int) ((Math.random() * maxX));
            int playerY = (int) ((Math.random() * maxY));

            if (units[playerX][playerY].getMyChar() == ' ') {
                this.player = new Player(playerX, playerY);
                units[playerX][playerY] = this.player;
                goodPlace = true;
            }

        } while (!goodPlace);

    }

    public void draw() {
        for (int i = 0; i < maxY; i++) {
            for (int j = 0; j < maxX; j++) {
                System.out.print(units[i][j].getMyChar());
            }
            System.out.println("");
        }
    }

    /*      int playerCoordinateX = (int)(Math.random() * (maxX - 2) + 1);
        int playerCoordinateY = (int)(Math.random() * (maxY - 2) + 1);
        units[playerCoordinateX][playerCoordinateX] = this.player;
        
        int monsterCoordinateX = (int)(Math.random() * (maxX - 2) + 1);
        int monsterCoordinateY = (int)(Math.random() * (maxY - 2) + 1);
        units[monsterCoordinateX][monsterCoordinateY] = new Monster(monsterCoordinateX, monsterCoordinateY);
     */
    public void speakRound() {
        System.out.println("The units speaking: ");
        for (int i = 0; i < maxY; i++) {
            for (int j = 0; j < maxX; j++) {
                units[i][j].speak();

            }
        }
        System.out.println("");
    }

    public void moveRound() {
        System.out.println("The units moving: ");
        for (int i = 0; i < maxY; i++) {
            for (int j = 0; j < maxX; j++) {
                units[i][j].move();

            }
        }
        System.out.println("");
    }

}
