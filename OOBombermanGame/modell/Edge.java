/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public class Edge extends Wall{

    public Edge(int xCoordinate, int yCoordinate) {
        super(xCoordinate,yCoordinate);
        this.infinite = true;
        this.MyChar = '|';
       
    }

    @Override
    public void speak() {
        System.out.println("I'm on the edge.");
    }
    
}
