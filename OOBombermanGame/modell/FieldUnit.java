/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public abstract class FieldUnit implements Speakable, Movable{
    
    protected int xCoordinate, yCoordinate;   
    protected boolean accessible;
    protected char MyChar;
    
    public void act() {}

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public char getMyChar() {
        return MyChar;
    }

    public void setMyChar(char MyChar) {
        this.MyChar = MyChar;
    }

    

   
    
    
    
}
