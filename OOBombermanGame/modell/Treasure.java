/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public class Treasure extends FieldUnit implements Speakable{
    private int value;

    public Treasure(int xCoordinate, int yCoordinate) {
         this.MyChar='T';
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.value = (int)(Math.random()* 10+1);
    }

    public int getValue() {
        return value;
    }

    
    
    @Override
    public void speak() {
        System.out.println("I'm a treasure, I have good value.");
    }

    @Override
    public void move() {
        System.out.println("I'm not moving");
    }
    
    
    
}
