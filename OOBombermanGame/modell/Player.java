/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public class Player extends FieldUnit implements Movable, Speakable{
    
     private int sumValue;
     private int sumMove;

    public Player(int xCoordinate, int yCoordinate) {
        this.MyChar='P';
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    } 
     
     public void putBomb() {}

    @Override
    public void move() {
        System.out.println("I'm the player and I move.");
    }

    @Override
    public void speak() {
        System.out.println("I'm the player and search treasures.");
    }
    
}
