/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public class Wall extends FieldUnit implements Speakable, Movable{
    protected boolean infinite = false;

    public Wall(int xCoordinate,int yCoordinate) {
        this.MyChar='#';
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        
    }
    
    private boolean isInfinite(){
        return (Math.random() < 0.5); 
            
        
    }
    
    

    @Override
    public void speak() {
        System.out.println("I'm a wall");
    }

    @Override
    public void move() {
        System.out.println("I'm not moving");
    }
    
    
}
