/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman.modell;

/**
 *
 * @author Norbi
 */
public class EmptyField extends FieldUnit implements Speakable{

    public EmptyField(int xCoordinate, int yCoordinate) {
        this.MyChar = ' ';
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        
    }
    
    @Override
    public void speak() {
        System.out.println("I'm empty.");
    } 

    @Override
    public void move() {
        System.out.println("I'm not moving");
    }
    
}
