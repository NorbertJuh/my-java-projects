package com.bh08.carproject.FinalExamCarProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.bh08.carproject.daos")
public class FinalExamCarProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[]{FinalExamCarProjectApplication.class, CarConfiguration.class}, args);
    }

}
