/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.models;

import java.time.LocalDateTime;

/**
 *
 * @author Norbi
 */
public class ChurchWedding {
    
    private Person wife;
    private Person husband;
    private LocalDateTime churchWeddingDate;
    private String sect;

    public ChurchWedding(Person wife, Person husband, LocalDateTime churchWeddingDate, String sect) {
        this.wife = wife;
        this.husband = husband;
        this.churchWeddingDate = churchWeddingDate;
        this.sect = sect;
    }

    
    
    public Person getWife() {
        return wife;
    }

    public void setWife(Person wife) {
        this.wife = wife;
    }

    public Person getHusband() {
        return husband;
    }

    public void setHusband(Person husband) {
        this.husband = husband;
    }

    public LocalDateTime getChurchWeddingDate() {
        return churchWeddingDate;
    }

    public void setChurchWeddingDate(LocalDateTime churchWeddingDate) {
        this.churchWeddingDate = churchWeddingDate;
    }

    public String getSect() {
        return sect;
    }

    public void setSect(String sect) {
        this.sect = sect;
    }
    
   
}
