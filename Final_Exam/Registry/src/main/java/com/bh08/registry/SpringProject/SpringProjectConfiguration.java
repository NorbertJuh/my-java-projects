/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.SpringProject;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author Norbi
 */
@Configuration
@ComponentScan(basePackages = "com.bh08.registry")
@EntityScan(basePackages = "com.bh08.registry")
public class SpringProjectConfiguration {


}
