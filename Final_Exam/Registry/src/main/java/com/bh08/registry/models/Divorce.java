/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.models;

import java.time.LocalDateTime;

/**
 *
 * @author Norbi
 */
public class Divorce {
    
    private Person wife;
    private Person husband;
    private LocalDateTime divorceDate;

    public Divorce(Person wife, Person husband, LocalDateTime divorceDate) {
        this.wife = wife;
        this.husband = husband;
        this.divorceDate = divorceDate;
    }

    public Person getWife() {
        return wife;
    }

    public void setWife(Person wife) {
        this.wife = wife;
    }

    public Person getHusband() {
        return husband;
    }

    public void setHusband(Person husband) {
        this.husband = husband;
    }

    public LocalDateTime getDivorceDate() {
        return divorceDate;
    }

    public void setDivorceDate(LocalDateTime divorceDate) {
        this.divorceDate = divorceDate;
    }
   
    
    
}
