/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.daos;

import com.bh08.registry.models.Death;
import com.bh08.registry.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Norbi
 */
@Repository
public interface DeathRepository extends JpaRepository<Death, Long> {
    
    
    
}
