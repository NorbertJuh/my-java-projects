/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.services;

/**
 *
 * @author Norbi
 */
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.registry.daos.PersonRepository;
import com.bh08.registry.models.Person;

@Service
public class RegistryService {
	@Autowired
	private PersonRepository personDao;

	public Person savePerson(Person person) {
		return personDao.saveAndFlush(person);
	}	
	
	public Optional<Person> getPersonById(Long id) {
		return personDao.findById(id);
	}	

}

