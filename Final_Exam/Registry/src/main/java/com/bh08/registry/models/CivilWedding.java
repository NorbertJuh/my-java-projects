/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.registry.models;

import java.time.LocalDateTime;

/**
 *
 * @author Norbi
 */
public class CivilWedding {
    
    private Person wife;
    private Person husband;
    private LocalDateTime civilWeddingDate;

    public CivilWedding(Person wife, Person husband, LocalDateTime civilWeddingDate) {
        this.wife = wife;
        this.husband = husband;
        this.civilWeddingDate = civilWeddingDate;
    }

    public Person getWife() {
        return wife;
    }

    public void setWife(Person wife) {
        this.wife = wife;
    }

    public Person getHusband() {
        return husband;
    }

    public void setHusband(Person husband) {
        this.husband = husband;
    }

    public LocalDateTime getCivilWeddingDate() {
        return civilWeddingDate;
    }

    public void setCivilWeddingDate(LocalDateTime civilWeddingDate) {
        this.civilWeddingDate = civilWeddingDate;
    }
    
    
}
