/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.controller;

import calculator.model.CalculatorModel;
import calculator.view.CalculatorView;

/**
 *
 * @author Toth.Attila
 */
public class CalculatorController {
    CalculatorModel calculatorModel;
    CalculatorView calculatorView;

    public CalculatorController() {
        calculatorModel = new CalculatorModel();
        calculatorView = new CalculatorView(this);
    }
    
    public void handlePlusButtonClick(Integer number) {
        
    }
    
    public void handleMinusButtonClick() {
        
    }
}
