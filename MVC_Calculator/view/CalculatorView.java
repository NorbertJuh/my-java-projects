/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import calculator.controller.CalculatorController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Toth.Attila
 */
public class CalculatorView extends JFrame implements ActionListener{
    private JFrame f;
    private JTextField t;
    private JButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,SubstractButton,addButton,equalButton,clear;
    private static double firstNumber=0,secondNumber=0,result=0;
    static int operator=0;
    private JTextField screen;
    private JPanel panel;
    private JButton plusButton;
    private JButton minusButton;
    private JButton[] numberButtons;
    
    CalculatorController calculatorController;
    
    public CalculatorView(CalculatorController calculatorController) {
        this.calculatorController = calculatorController;
        initialize();
        
        ActionListener plusAction = p -> {
            calculatorController.handlePlusButtonClick(Integer.valueOf(screen.getText()));
        };
        plusButton = new JButton("+");
        plusButton.addActionListener(plusAction);          
        
    }
    
    private void initialize(){
        //setUpWindow();
        //setUpCalculatorScreen();
        setupNumbers();
        
        showWindow();
        
    }
    
    public void showWindow() {
        //this.pack();
        this.setVisible(true);
    }

    private void setUpWindow() {
        f.setSize(500, 500);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setTitle("Calculator");
        f.setLayout(null);
        f.setVisible(true);
        f.setResizable(false);

       /* setLayout(new GridLayout(5, 5));
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(panel);

        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);*/
    }
    
    private void setUpCalculatorScreen() {
        this.screen = new JTextField("0");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.screen.setHorizontalAlignment(JTextField.RIGHT);
        this.screen.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(screen, gbc);
    }
    
    private void setupNumbers(){
            
        f=new JFrame("Calculator");       
        t=new JTextField();
        b1=new JButton("1");
        b2=new JButton("2");
        b3=new JButton("3");
        b4=new JButton("4");
        b5=new JButton("5");
        b6=new JButton("6");
        b7=new JButton("7");
        b8=new JButton("8");
        b9=new JButton("9");
        b0=new JButton("0");       
        SubstractButton=new JButton("-");
        addButton=new JButton("+");       
        equalButton=new JButton("=");
        clear=new JButton("C");
        
        t.setBounds(30,40,280,30);
        b7.setBounds(40,100,70,60);
        b8.setBounds(110,100,70,60);
        b9.setBounds(180,100,70,60);
        SubstractButton.setBounds(250,100,70,60);
        
        b4.setBounds(40,170,70,60);
        b5.setBounds(110,170,70,60);
        b6.setBounds(180,170,70,60);
        addButton.setBounds(250,170,70,60);
        
        b1.setBounds(40,240,70,60);
        b2.setBounds(110,240,70,60);
        b3.setBounds(180,240,70,60);
        equalButton.setBounds(250,240,70,60);
        
       
        b0.setBounds(110,310,70,60); 
        clear.setBounds(180,310,70,60);
        
        f.add(t);
        f.add(b7);
        f.add(b8);
        f.add(b9);
       
        f.add(b4);
        f.add(b5);
        f.add(b6);
       
        f.add(b1);
        f.add(b2);
        f.add(b3);
        f.add(SubstractButton);
        
        f.add(b0);
        f.add(equalButton);
        f.add(addButton);
      
        f.add(clear);
        setUpWindow();
      
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        b0.addActionListener(this);
        addButton.addActionListener(this);
       
        SubstractButton.addActionListener(this);
       
        equalButton.addActionListener(this);

        clear.addActionListener(this);
    
    }
    
 /*   private void setupPlusButton() {
        GridBagConstraints gbc;
        this.plusButton = new JButton("+");
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(plusButton, gbc);
        
    }*/

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==b1)
            t.setText(t.getText().concat("1"));
        
        if(e.getSource()==b2)
            t.setText(t.getText().concat("2"));
        
        if(e.getSource()==b3)
            t.setText(t.getText().concat("3"));
        
        if(e.getSource()==b4)
            t.setText(t.getText().concat("4"));
        
        if(e.getSource()==b5)
            t.setText(t.getText().concat("5"));
        
        if(e.getSource()==b6)
            t.setText(t.getText().concat("6"));
        
        if(e.getSource()==b7)
            t.setText(t.getText().concat("7"));
        
        if(e.getSource()==b8)
            t.setText(t.getText().concat("8"));
        
        if(e.getSource()==b9)
            t.setText(t.getText().concat("9"));
        
        if(e.getSource()==b0)
            t.setText(t.getText().concat("0"));
        
       if(e.getSource()==addButton)
        {
            firstNumber=Double.parseDouble(t.getText());
            operator=1;
            t.setText("");
        } 
        
        if(e.getSource()==SubstractButton)
        {
            firstNumber=Double.parseDouble(t.getText());
            operator=2;
            t.setText("");
        }
        
        if(e.getSource()==clear)
            t.setText("");
        
        if(e.getSource()==equalButton)
        {
            secondNumber=Double.parseDouble(t.getText());
        
            switch(operator)
            {
                case 1: result=firstNumber+secondNumber;
                    break;
        
                case 2: result=firstNumber-secondNumber;
                    break;
                    
                default: result=0;
            }
        
            t.setText("" + result);
        }
    }
    
}
