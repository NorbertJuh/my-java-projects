/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;

/**
 *
 * @author Norbi
 */
public class Banking {


   
    public static void main(String[] args) {
        User bela = new User("Béla");
        User Jozsef = new User("Józsi");
        User Geza = new User("Géza");
        
        BankAccount belaAccount = new RetailAccount("12345678-00000000", bela, BigDecimal.valueOf(3000000));
        BankAccount jozsiAccount = new RetailAccount("23459678-00000000", Jozsef, BigDecimal.valueOf(1000));
        BankAccount gezaAccount = new StudentAccount("62349678-00000000", Geza, BigDecimal.valueOf(500));
        
        TransferService.transferMoney("pocketmoney", belaAccount, jozsiAccount, BigDecimal.valueOf(10000));
        System.out.println(belaAccount);
        System.out.println(jozsiAccount);
        System.out.println(gezaAccount);
        System.out.println("Béla account balance: " + belaAccount.getAccountBalance());
        System.out.println(jozsiAccount.getAccountBalance());    
        
    }
    
}
