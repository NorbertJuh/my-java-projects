/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;

/**
 *
 * @author Norbi
 */
public class TransferService {

    public static void transferMoney(String message, BankAccount from, BankAccount to, BigDecimal money) {
        Transaction transaction = new Transaction(message, money, from, to);
        if (from.getAccountBalance().compareTo(money) >= 0) {  //
            from.addTransaction(transaction);
            to.addTransaction(transaction);

        }

    }
}
