/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Norbi
 */
public abstract class BankAccount {

    protected String accountNumber;
    protected User user;
    protected BigDecimal accountBalance;
    private static BigDecimal accountOpenFee = BigDecimal.valueOf(500);
    protected List<Transaction> transactionList = new ArrayList<>();

    public BankAccount(String accountNumber, User user, BigDecimal accountBalance) {
        this.accountNumber = accountNumber;
       this.user = user;
        this.accountBalance = accountBalance.subtract(accountOpenFee);
        Transaction transaction = new Transaction("deducting account opening fee.", accountOpenFee, this, null);
    }

    public final void addTransaction(Transaction transaction) {

        if (transaction.getSourceAccount() != null && transaction.getSourceAccount().equals(this)) {

            this.accountBalance = this.accountBalance.subtract(transaction.getValue());

        } else if (transaction.getDestinationAccount() != null && transaction.getDestinationAccount().equals(this)) {

            this.accountBalance = this.accountBalance.add(transaction.getValue());

        } else {
            throw new RuntimeException("This transaction does not belong to the current bankaccount");
        }
        this.transactionList.add(transaction);
    }

    public abstract void deductAccountFee();

    public abstract void getTransferFee();

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    //   public static BigDecimal getAccountOpenFee() {
    //       return accountOpenFee; }

    @Override
    public String toString() {
        return "BankAccount{" + "accountNumber=" + accountNumber + ", user=" + user.getName() + ", accountBalance=" + accountBalance + '}';
    }
    
    
}

