/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;

/**
 *
 * @author Norbi
 */
public class RetailAccount extends BankAccount {

    private static final BigDecimal monthlyAccountFeeNormal = BigDecimal.valueOf(1500);
    private static final BigDecimal monthlyAccountFeeDiscount = BigDecimal.valueOf(500);

    public RetailAccount(String accountNumber, User user, BigDecimal accountBalance) {
        super(accountNumber, user, accountBalance);
    }

    @Override
    public void deductAccountFee() {
        int currentbalance;
        currentbalance = accountBalance.compareTo(BigDecimal.valueOf(250000));
        if (currentbalance == 1) {
            accountBalance = accountBalance.subtract(monthlyAccountFeeDiscount);
        } else if (currentbalance == 0) {
            accountBalance = accountBalance.subtract(monthlyAccountFeeNormal);
        } else if (currentbalance == -1) {
            accountBalance = accountBalance.subtract(monthlyAccountFeeNormal);
        }
    }

  
    public void getTransferFee() {
            
   accountBalance=accountBalance.multiply(BigDecimal.valueOf(1.05));
  
    }

}
