/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author Norbi
 */
public class Transaction {
    
    private static int nextId = 1;
    private int id;
    private LocalDateTime timeStamp;
    private String description;
    private BigDecimal value;
    private BankAccount sourceAccount, destinationAccount;

    public Transaction(String description, BigDecimal value, BankAccount sourceAccount, BankAccount destinationAccount) {
        this.id = nextId;
        nextId++;
        this.timeStamp = LocalDateTime.now();
        this.description = description;
        this.value = value;
        this.sourceAccount = sourceAccount;
        this.destinationAccount = destinationAccount;
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public BankAccount getSourceAccount() {
        return sourceAccount;
    }

    public BankAccount getDestinationAccount() {
        return destinationAccount;
    }

    @Override
    public String toString() {
        return "Transaction{" + "id=" + id + ", timeStamp=" + timeStamp + ", description=" + description + ", value=" + value + ", sourceAccount=" + sourceAccount + ", destinationAccount=" + destinationAccount + '}';
    }
    
    
    
}
