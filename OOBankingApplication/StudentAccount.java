/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.juhasznorbert.banking;

import java.math.BigDecimal;

/**
 *
 * @author Norbi
 */
public class StudentAccount extends BankAccount{

    public StudentAccount(String accountNumber, User user, BigDecimal accountBalance) {
        super(accountNumber, user, accountBalance);
    }

    @Override
    public void deductAccountFee() {
        
    }

    @Override
    public void getTransferFee() {
    }
    
}
