/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.controller;

import employeemanager.model.Employee;
import employeemanager.view.ConsoleView;
import employeemanager.view.IView;
import employeemanager.view.SwingView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Norbi
 */
public class EmployeeController implements ViewInteraction{
    
    private IView view;
    public List<Employee> employees;
    private int lastId = 0;

    public EmployeeController() {
        this.employees = new ArrayList<>();
        generateEmployees();
    }
    
    public void initView(int viewNumber){
        
        if (viewNumber == 0)
            view = new ConsoleView();
        else 
            view = new SwingView();
        
        view.setController(this);
        view.showMenu();
    }
    
    private void generateEmployees(){
        for(int i = 0; i < 10; i++){
            Employee em = new Employee(i, "Employee" + i, 100000);
            employees.add(em);
            lastId++;
        }
    }

    @Override
    public void list() {
        view.showEmployeeList(employees.toString());
    }

    @Override
    public void add(String name, int salary) {
        Employee em = new Employee(++lastId, name, salary);
        employees.add(em);
        view.showMessage("" + em.getId() + " successfully created!");
    }

    @Override
    public void remove(int id) {
        employees.remove(id);
        view.showMessage("" + id + "successfully removed!");
        //view.showMenu();
    }

    @Override
    public void getEmployee(int id) {
        view.showEmployee(employees.get(id).toString());
    }

    @Override
    public void doSelected(int selected) {
        switch(selected) {
            case 1: list(); break;
            case 2: view.doMenuGetEmployee(); break;
            case 3: view.doMenuRemoveEmployee(); break;
            case 4: view.doMenuAddEmployee(); break;
            
        }
    }
    
    
}

