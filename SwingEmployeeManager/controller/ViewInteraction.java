/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.controller;

/**
 *
 * @author Norbi
 */
public interface ViewInteraction {
      
    public void doSelected(int selected);
    public void getEmployee(int id);
    public void remove(int id);
    public void add(String name, int salary);
    public void list();

    
}
