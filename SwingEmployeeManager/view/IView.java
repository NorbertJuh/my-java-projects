/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.view;

import employeemanager.controller.EmployeeController;

/**
 *
 * @author Norbi
 */
public interface IView {
    
    public void showMenu();
    public void setController(EmployeeController con);
    public void showEmployee(String employeeString);
    public void showEmployeeList(String listString);
    public void showMessage(String text);
    public void doMenuGetEmployee();
    public void doMenuRemoveEmployee();
    public void doMenuAddEmployee();
}
