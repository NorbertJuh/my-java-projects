/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanager.view;

import employeemanager.controller.EmployeeController;
import java.util.Scanner;

/**
 *
 * @author Norbi
 */
public class ConsoleView implements IView{
    
    EmployeeController con;

    @Override
    public void setController(EmployeeController con) {
        this.con = con;
    }

    @Override
    public void showEmployee(String employeeString) {
        System.out.println(employeeString);
    }

    @Override
    public void showEmployeeList(String listString) {
        
        System.out.println(listString);
    }

    @Override
    public void showMessage(String text) {
        System.out.println(text);
    }

    @Override
    public void showMenu() {
       
        int menuNumber;
        
        do{
            menuNumber = extra.Console.readInt("Choose a menu number: (5 Quit)\n"
                    + "1. list all employees\n"
                    + "2. get employee by id\n"
                    + "3. remove employee by id\n"
                    + "4. add employee");
            
        switch (menuNumber){
            case 1:      
            con.doSelected(1); break;
            case 2:
            con.doSelected(2); break;
            case 3:      
            con.doSelected(3); break;
            case 4:
            con.doSelected(4); break;
            
        }
        
    }while(menuNumber!=5);
        }

    @Override
    public void doMenuGetEmployee() {
        int employeeNumber = extra.Console.readInt("Choose an employee by id: ");
        con.getEmployee(employeeNumber);
    }

    @Override
    public void doMenuRemoveEmployee() {
        int employeeDelete = extra.Console.readInt("Choose an employee to delete by id: ");
        con.remove(employeeDelete);
    }

    @Override
    public void doMenuAddEmployee() {
        String employeeName = extra.Console.readLine("Type the name of the new employee: ");
        con.add(employeeName, 100000);
    }
    
}
